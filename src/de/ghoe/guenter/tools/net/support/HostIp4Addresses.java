package de.ghoe.guenter.tools.net.support;

import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class HostIp4Addresses
{
	private HostIp4Addresses()
	{
		m_AddrList = new LinkedList< NetInfo >();
		try
		{
			// need to collect all known addresses
			for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); )
			{
				NetworkInterface ni = en.nextElement();
				List<InterfaceAddress> li = ni.getInterfaceAddresses();
				for (Iterator<InterfaceAddress> ii = li.iterator(); ii.hasNext(); )
				{
					InterfaceAddress ia = ii.next();

					if (NetInfo.s_isIp4( ia))
					{
						NetInfo info = new NetInfo( ia);
						m_AddrList.add( info);
					}
				}
			}
		}
		catch( Throwable e)
		{
			System.err.println( "Failed to get Network-Inteface: " + e.getMessage());
		}
	}
	
	public static ArrayList<String> s_getHostsAddresses()
	{
		ArrayList<String> list = new ArrayList<String>();		
		list.add("any");
		
		try
		{
			for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); )
			{
				NetworkInterface ni = en.nextElement();
				List<InterfaceAddress> li = ni.getInterfaceAddresses();
				for (Iterator<InterfaceAddress> ii = li.iterator(); ii.hasNext(); )
				{
					InterfaceAddress ia = ii.next();
	
					if (NetInfo.s_isIp4( ia))
					{
						InetAddress ina = ia.getAddress();
						String sa = ina.toString();
						// System.out.println( "Detected IP4 Address: " + sa);
						list.add( sa);
						break;
					}
				}
			}
		}
		catch( Throwable e)
		{
			System.err.println( "Failed to get Network-Inteface: " + e.getMessage());
		}
		return list;
	}
	
	public static String[] s_getHostsAddressArray()
	{
		ArrayList<String> l = s_getHostsAddresses();
		String[] a = new String[ l.size()];
		l.toArray( a);
		return a;		
	}
	
	public static int s_getBestHostAddress( int ip)
	{
		return s_instance().getBestHostAddress( ip);
	}
	
	public static int s_getAddressMask( int ip)
	{
		return s_instance().getAddressMask( ip);
	}
	
	public int getBestHostAddress( int ip)
	{
		for (Iterator<NetInfo> it = m_AddrList.iterator(); it.hasNext();)
		{
			NetInfo ni = it.next();
			if (ni.matchIp4(ip))
			{
				return ni.getIp4Addr();
			}
		}
		return 0;
	}
	
	public int getAddressMask( int ip)
	{
		for (Iterator<NetInfo> it = m_AddrList.iterator(); it.hasNext();)
		{
			NetInfo ni = it.next();
			if (ni.matchIp4(ip))
			{
				return ni.getIp4Mask();
			}
		}
		return 0;
	}
		
	public static HostIp4Addresses s_instance()
	{
		if (null == s_Instance)
		{
			s_createInstance();
		}
		return s_Instance;
	}
	
	private static synchronized void s_createInstance()
	{
		if (null == s_Instance)
		{
			s_Instance = new HostIp4Addresses();
		}
	}
		
	private List< NetInfo >			m_AddrList;
	private static HostIp4Addresses	s_Instance;
}
