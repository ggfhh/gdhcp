package de.ghoe.guenter.tools.net.support;

public class Ip6Util
{
	public static void s_getIp6Addr( byte[] out, String in) throws Exception
	{
		String[] data = in.split("\\s*\\:\\s*");
		
		if (data.length != 4) throw new Exception( "Invalid IP6-Address-Value: " + in);
		
		for (int i = 0; i < 8; ++i)
		{
			int val = Integer.parseInt(data[i], 16);
			out[2*i] = (byte) (val >> 8);
			out[2*i+1] = (byte) val;
		}
	}
}
