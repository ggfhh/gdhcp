package de.ghoe.guenter.tools.net.support;

public class MacUtil
{
	public static String s_getMacString( long mac)
	{
		return String.format( "%02x:%02x:%02x:%02x:%02x:%02x", ((mac >> 40) & 0xff), ((mac >> 32) & 0xff), ((mac >> 24) & 0xff), ((mac >> 16) & 0xff), ((mac >> 8) & 0xff), ((mac >> 0) & 0xff));
	}
	
	public static long s_getMacAddr( String in) throws Exception
	{
		System.out.println( "Input=" + in);
		String out = in.replaceAll( ":(.):", ":0$1:");	// make all numbers to two digits :-)
		System.out.println( "\t==> " + out);
		out = out.replaceFirst( "^\\s*(.):", "0$1:");
		System.out.println( "\t==> " + out);
		out = out.replaceFirst( ":(.)\\s*$", ":0$1");
		System.out.println( "\t==> " + out);
		out = out.replaceAll( ":",  "");				// remove the :
		System.out.println( "\t==> " + out);
		long v = Long.parseLong(out, 16);; 
		System.out.printf( "\t==> 0x" + Long.toHexString(v) + " ==> " + v);
		return v;
	}
}
