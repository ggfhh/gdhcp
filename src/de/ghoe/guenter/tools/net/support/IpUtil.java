package de.ghoe.guenter.tools.net.support;

public class IpUtil 
{
	public static int setVal( byte[] data, int off, short val)
	{
		data[off] = (byte) (val >> 8);
		data[++off] = (byte) val;
		return ++off;
	}
	
	public static int setVal( byte[] data, int off, int val)
	{
		data[off] = (byte) (val >> 24);
		data[++off] = (byte) (val >> 16);
		data[++off] = (byte) (val >> 8);
		data[++off] = (byte) val;
		return ++off;
	}
	
	public static int setVal( byte[] data, int off, long val)
	{
		data[off] = (byte) (val >> 56);
		data[++off] = (byte) (val >> 48);
		data[++off] = (byte) (val >> 40);
		data[++off] = (byte) (val >> 32);
		data[++off] = (byte) (val >> 24);
		data[++off] = (byte) (val >> 16);
		data[++off] = (byte) (val >> 8);
		data[++off] = (byte) val;
		return ++off;
	}
	
	public static int setVal( byte[] data, int off, byte[] fill)
	{
		System.arraycopy( fill, 0, data, off, fill.length);
		return off + fill.length;
	}
	
	public static int setVal( byte[] data, int off, String s)
	{
		byte[] fill = s.getBytes();
		System.arraycopy( fill, 0, data, off, fill.length);
		data[off+fill.length] = 0; // termination
		return off + fill.length + 1;
	}
	
	public static String getString( byte[] data, int off)
	{
		int i = off;
		while( (i < data.length) && (0 != data[i]))
		{
			++i;
		}
		String res = new String( data, off, i);
		return res;
	}
	
	public static short getUByte( byte[] data, int off)
	{
		short v = data[off];
		if (0 > v) v += 256;
		return v;
	}
	
	public static int getUShort( byte[] data, int off)
	{
		int v = getUByte( data, off);
		v <<= 8;
		v += getUByte( data, ++off);
		return v;		
	}
	public static short getShort( byte[] data, int off)
	{
		return (short) getUShort( data, off);
	}
	
	public static long getUInt( byte[] data, int off)
	{
		long v = getUByte( data, off);
		v <<= 8;
		v += getUByte( data, ++off);
		v <<= 8;
		v += getUByte( data, ++off);
		v <<= 8;
		v += getUByte( data, ++off);		
		return v;
	}
	public static int getInt( byte[] data, int off)
	{
		return (int) getUInt( data, off);
	}
	
	public static long getLong( byte[] data, int off)
	{
		long v = getUByte( data, off);
		for (int i = 0; i < 7; ++i)
		{
			v <<= 8;
			v += getUByte( data, ++off);
		}
		return v;
	}
}
