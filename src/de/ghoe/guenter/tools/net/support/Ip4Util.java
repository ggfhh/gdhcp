package de.ghoe.guenter.tools.net.support;

import java.net.InetAddress;

public class Ip4Util
{
	public static String s_getIp4String( int addr)
	{
		return String.format( "%d.%d.%d.%d", sp_Int2Byte( addr, 24), sp_Int2Byte( addr, 16), sp_Int2Byte( addr, 8), sp_Int2Byte( addr, 0));
	}	
	
	public static int s_getIp4Addr( String in) throws Exception
	{
		String t = in.replaceFirst( "[\\s-|/]+.*", "");
		return s_getIp4Value( t);
	}
	
	public static int s_getIp4Mask( String in, int def) throws Exception
	{
		String t = in.replaceFirst( ".*[\\s-|/]+", "");
		if (t.isEmpty())
			return def;
		return s_getIp4Value( t);
	}
	
	public static int s_getIp4Addr( InetAddress address)
	{	
		byte[] addr = address.getAddress();
	
		return s_getIp4Addr( addr);
	}
	
	public static int s_getIp4Addr( byte[] addr)
	{
		int ip = sp_Byte2Int( addr[0], 24);
		ip  |= sp_Byte2Int( addr[1], 16);
		ip  |= sp_Byte2Int( addr[2], 8);
		ip  |= sp_Byte2Int( addr[3], 0);	
		
		return ip;		
	}
	
	public static void s_getIp4Addr( byte[] out, String in) throws Exception
	{
		String[] data = in.split("\\s*\\.\\s*");
		
		if (data.length != 4) throw new Exception( "Invalid IP4-Address-Value: " + in);
		
		out[0] = (byte) sp_getIpByte( data[0], 0);
		out[1] = (byte) sp_getIpByte( data[1], 0);
		out[2] = (byte) sp_getIpByte( data[2], 0);
		out[3] = (byte) sp_getIpByte( data[3], 0);
	}
	
	public static int s_getIp4Value( String in) throws Exception
	{
		String[] data = in.split("\\s*\\.\\s*");
		
		if (data.length != 4) throw new Exception( "Invalid IP4-Address-Value: " + in);
		
		int v = sp_getIpByte( data[0], 24);
		v += sp_getIpByte( data[1], 16); 
		v += sp_getIpByte( data[2], 8);
		v += sp_getIpByte( data[3], 0);
		
		return v;
	}

	private static int sp_getIpByte( String in, int shift) throws Exception
	{
		int v = Integer.parseInt( in);
		
		if (255 < v) throw new Exception( "IP4-Byte " + v + " is out of range");		
		return v << shift;
	}
	
	private static int sp_Int2Byte( int in, int shift)
	{
		int out = in >> shift;
		out &= 0xff;
		return out;		
	}
	
	private static int sp_Byte2Int( byte in, int shift)
	{
		int out = (0 <= in) ? in : in + 256;
		
		out <<= shift;
		return out;
	}
}
