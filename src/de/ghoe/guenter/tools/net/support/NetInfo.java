package de.ghoe.guenter.tools.net.support;

import java.net.InetAddress;
import java.net.InterfaceAddress;

public class NetInfo
{
	public NetInfo( InterfaceAddress ia)
	{
		m_InterfaceAddr = ia;
		m_IsIp4 = s_isIp4( m_InterfaceAddr);
		if (m_IsIp4)
		{
			InetAddress addr = ia.getAddress();
			byte[] ta = addr.getAddress();
			m_Ip4Addr = Ip4Util.s_getIp4Addr( ta);
			int prefixLen = m_InterfaceAddr.getNetworkPrefixLength();
			m_Ip4Mask = -1 << (32-prefixLen);
			m_Ip4Cmp  = m_Ip4Addr & m_Ip4Mask;
		}
	}
	
	public static boolean s_isIp4( InterfaceAddress ia)
	{
		InetAddress addr = ia.getAddress();
		byte[] bytes = addr.getAddress();
		int len = bytes.length;
		
		return (IPV4_ADDRLEN == len);
	}
	
	public boolean isIp4()
	{
		return m_IsIp4;
	}
	
	public int getIp4Addr()
	{
		return m_Ip4Addr;
	}
	
	public int getIp4Mask()
	{
		return m_Ip4Mask;
	}
	
	public boolean matchIp4( int addr)
	{
		int cmp = (addr & m_Ip4Mask);
		return m_Ip4Cmp == cmp;
	}
	
	private InterfaceAddress	m_InterfaceAddr;
	private boolean				m_IsIp4;
	private int					m_Ip4Addr;
	private int					m_Ip4Cmp;
	private int					m_Ip4Mask;

	private static final int			IPV4_ADDRLEN = 4;
}
