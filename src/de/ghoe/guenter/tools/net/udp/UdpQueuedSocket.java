package de.ghoe.guenter.tools.net.udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.DatagramSocketImpl;
import java.net.InetAddress;
import java.net.SocketAddress;
import java.net.SocketException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.concurrent.Semaphore;

import de.ghoe.guenter.support.Log;
import de.ghoe.guenter.tools.net.udp.support.UdpQueuedPacket;

public class UdpQueuedSocket extends DatagramSocket 
{
	/////////////////////////////////////////////////////////////////////////////////
	// constructors
	/////////////////////////////////////////////////////////////////////////////////
	public UdpQueuedSocket( int queueSize) throws SocketException 
	{
		super();
		p_Init( queueSize);
	}

	public UdpQueuedSocket(int queueSize, DatagramSocketImpl impl) 
	{
		super(impl);
		p_Init( queueSize);
	}

	public UdpQueuedSocket(int queueSize, SocketAddress addr) throws SocketException 
	{
		super( addr);
		p_Init( queueSize);
	}

	public UdpQueuedSocket(int queueSize, int port) throws SocketException 
	{
		super( port);
		p_Init( queueSize);
	}

	public UdpQueuedSocket(int queueSize, int port, InetAddress addr) throws SocketException 
	{
		super( port, addr);
		p_Init( queueSize);
	}
	
	/////////////////////////////////////////////////////////////////////////////////
	// methods
	/////////////////////////////////////////////////////////////////////////////////
	public boolean canEnqueue()
	{
		return 0 < m_QueueAvailSem.availablePermits();
	}
	
	@Override
	public void send( DatagramPacket p) throws IOException
	{
		try
		{
			if (m_UseQueue)
			{
				Log.logVerbose( "Wait for queue has space to enqueu packet to send");
				// 1. wait for space in queue
				m_QueueAvailSem.acquire();
				
				if (!super.isClosed())
				{
					// 2. send frame
	//				super.send( p);
					Log.logWarning( "Skipp sending of packet!");
					// 3. enqueue
					p_enqueue( new UdpQueuedPacket( p));
				}
				else
				{
					Log.logWarning( "Socket is closed drop packet to send!");				
				}
			}
			else
			{
				super.send( p);
				Log.logVerbose( "Sending packet without queue!");				
			}
		}
		catch( InterruptedException e)
		{
			Log.log( "Interrupt on send packet", e);
		}
	}
	
	public void sendUnqueued( DatagramPacket p) throws IOException
	{
//		System.out.println( "Send unqueued!");
//		super.send( p);
		Log.logWarning( "Skipp sending of packet!");
	}
	
	public boolean isSendFinished()
	{
		boolean res;
		synchronized( m_PacketList)
		{	// can send next frame
			res = m_PacketList.isEmpty();
		}
		return res;
	}	
	
	public void confirmPacket()
	{
		synchronized( m_PacketList)
		{	// can send next frame
			p_freeEntrySave();
		}
	}
	
	public void dropPacket()
	{
		synchronized( m_PacketList)
		{
			p_freeEntrySave();
		}	
	}
	
	public void clearQueue()
	{
		synchronized( m_PacketList)
		{	// can send next frame
			while (!m_PacketList.isEmpty())
			{
				p_freeEntrySave();
			}	
		}
	}
	
	public void close()
	{
		super.close();		
		clearQueue();
	}
	
	public void resendPackets() throws IOException
	{
		synchronized( m_PacketList)
		{
			for (Iterator<UdpQueuedPacket> it = m_PacketList.iterator(); it.hasNext();)
			{
				UdpQueuedPacket e = it.next();
				DatagramPacket p = e.getPacket();
				Log.logWarning( "Skipp resending of packet!");
				// s( p);
			}			
		}
	}
	
	/////////////////////////////////////////////////////////////////////////////////
	// privates
	/////////////////////////////////////////////////////////////////////////////////
	private void p_enqueue( UdpQueuedPacket p)
	{
		synchronized( m_PacketList)
		{
			m_PacketList.add( p);
		}
	}

	private void p_freeEntrySave()
	{
		m_QueueAvailSem.release();
		m_PacketList.pop();						
	}

	private void p_Init( int queueSize)
	{
		m_QueueAvailSem = new Semaphore( queueSize);
		m_PacketList    = new LinkedList<UdpQueuedPacket>();
		m_UseQueue = (0 != queueSize);
	}
	
	private LinkedList<UdpQueuedPacket>	m_PacketList;
	private boolean						m_UseQueue;
	private Semaphore					m_QueueAvailSem;
}
