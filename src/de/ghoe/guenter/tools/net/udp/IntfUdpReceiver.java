package de.ghoe.guenter.tools.net.udp;

import java.net.DatagramPacket;

public interface IntfUdpReceiver
{
	public static final int MAX_UDP_BUFFER_SIZE	= 0x10000;
	
	public boolean udpRxStartup();
	public boolean udpRxHandle( DatagramPacket p);
	public boolean udpRxTimeout();
	public void udpRxAbort();
	public void udpRxShutdown();
}
