package de.ghoe.guenter.tools.net.udp.dhcp.options.base;

public class DhcpOptionNone extends DhcpOption
{
	protected DhcpOptionNone( byte id, String name)
	{
		super( id, name);
	}
	
	protected DhcpOptionNone( byte id, String name, OptionParseInfo info)
	{
		super( id, name);
	}

	protected DhcpOptionNone( byte id, String name, String val) throws Exception
	{
		super( id, name);
	}
	
	@Override
	public byte[] getBytes()
	{
		byte[] b = new byte[1];		
		b[0] = getId();
		return b;
	}
	

	@Override
	public int getLen()
	{
		return 1;	/* only cmd */
	}

	@Override
	public int insertData(byte[] buf, int pos)
	{
		buf[pos] = getId();
		return 1;
	}

}
