package de.ghoe.guenter.tools.net.udp.dhcp.options.base;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.ghoe.guenter.tools.net.udp.dhcp.options.*;

public class FactoryDhcpOption
{
	private FactoryDhcpOption()
	{
		m_OptionClasses = new HashMap<Byte,Constructor<?> >();
		m_OptionNames   = new HashMap<String,Constructor<?> >();

		createOptionEntry( DhcpOptionBootFileSize.class);
		createOptionEntry( DhcpOptionBroadcastAddress.class);
		createOptionEntry( DhcpOptionClientIdentification.class);
		createOptionEntry( DhcpOptionCookieServer.class);
		createOptionEntry( DhcpOptionDomainName.class);
		createOptionEntry( DhcpOptionDomainNameServer.class);
		createOptionEntry( DhcpOptionEnd.class);
		createOptionEntry( DhcpOptionExtensionsPath.class);
		createOptionEntry( DhcpOptionHostName.class);
		createOptionEntry( DhcpOptionImpressServer.class);
		createOptionEntry( DhcpOptionIpLeaseTime.class);
		createOptionEntry( DhcpOptionLogServer.class);
		createOptionEntry( DhcpOptionLprServer.class);
		createOptionEntry( DhcpOptionMeritDumpFile.class);
		createOptionEntry( DhcpOptionMessageType.class);
		createOptionEntry( DhcpOptionNameServer.class);
		createOptionEntry( DhcpOptionNtpServer.class);
		createOptionEntry( DhcpOptionPad.class);
		createOptionEntry( DhcpOptionParameterRequestList.class);
		createOptionEntry( DhcpOptionRebindTime.class);
		createOptionEntry( DhcpOptionRenewalTime.class);
		createOptionEntry( DhcpOptionResourceLocationServer.class);
		createOptionEntry( DhcpOptionRootPath.class);
		createOptionEntry( DhcpOptionRouter.class);
		createOptionEntry( DhcpOptionServerIdentification.class);
		createOptionEntry( DhcpOptionSubnetMask.class);
		createOptionEntry( DhcpOptionSwapServer.class);
		createOptionEntry( DhcpOptionTimeOffset.class);
		createOptionEntry( DhcpOptionTimeServer.class);		
	}
	
	public static FactoryDhcpOption s_Instance()
	{
		if (null == s_Instance)
		{
			s_createInstance();			
		}
		return s_Instance;
	}
	
	public static int s_addOptions( ArrayList<DhcpOption> al, byte[] buffer, int off, int len)
	{
		return s_Instance().addOptions( al,  buffer, off, len);
	}
	
	public static DhcpOption s_createOption( String in)
	{
		return s_Instance().createOption( in);
	}

	public int addOptions( ArrayList<DhcpOption> al, byte[] buffer, int off, int len)
	{
		OptionParseInfo info = new OptionParseInfo( buffer, off, len);
		
		for (DhcpOption option = createOptions( info); null != option;option = createOptions( info)) 
		{
			al.add( option);
		}
		int pos = info.getOffset();
		
		return pos;
	}
	
	public DhcpOption createOption( String in)
	{	// format: <name>: {<data>}
		DhcpOption res = null;
		String name = in.replaceFirst( "\\s*:.*", "");
		
		for (Map.Entry<String, Constructor<?> > entry : m_OptionNames.entrySet()) 
		{
		    String key = entry.getKey();
		    
		    if (name.equalsIgnoreCase( key))
		    {
		    	String val = in.replaceFirst( "[^:]*:\\s*", "");
		    	Constructor<?> ctor = entry.getValue();
		    	try
				{
					res = (DhcpOption) ctor.newInstance( val);
					break;
				} 
		    	catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e)
				{
				}
		    }
		}
		return res;
	}
	
	public DhcpOption createOption( byte[] buf, int off, int len)
	{		
		OptionParseInfo info = new OptionParseInfo( buf, off, len);
		return createOptions( info);
	}
	
	public DhcpOption createOptions( OptionParseInfo info)
	{
		while( info.hasData())
		{
			Byte id = info.readId();
//			System.out.println( "Detected Option " + id);
			try
			{			
				Constructor<?> ctor = m_OptionClasses.get( id);
				if (null != ctor)
				{
//					System.out.println( "Found known option " + id);
					DhcpOption nopt;
					nopt = (DhcpOption) ctor.newInstance( info);
					if (nopt.lastOption())
					{
						info.close();
					}
					return nopt;
				}
				else
				{
					info.skippData();
				}
			}
			catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e)
			{
				// ignore unknown options
			}
		}
		return null;
	}
	
	private static synchronized void s_createInstance( )
	{
		if (null == s_Instance)
		{	// need to recheck, since first check was not synchronized
			s_Instance = new FactoryDhcpOption(); 
		}
	}

	private void createOptionEntry( Class<?> clazz)
	{
		try
		{
			Field idfield_t = clazz.getField( "TYPE_ID");
			Field idfield_n = clazz.getField( "NAME_ID");
			Class<?> t = idfield_t.getType();
			Class<?> n = idfield_n.getType();
			
			if ((t == Byte.class) && (n == String.class))
			{
				Byte id = (Byte) idfield_t.get(null);
				String name = (String) idfield_n.get(null);
	
//				System.out.println( "Create option: " + id + " -> "+ name);
				
				Constructor<?> ctor_t = clazz.getConstructor( OptionParseInfo.class);
				Constructor<?> ctor_n = clazz.getConstructor( String.class);
				
				m_OptionClasses.put( id, ctor_t);
				m_OptionNames.put( name,  ctor_n);
			}
		}
		catch( Throwable e)
		{
			System.err.println( "Failed to add OptionEntry: " + e.getLocalizedMessage());
		}
	}

	HashMap<Byte,Constructor<?> >	m_OptionClasses;
	HashMap<String,Constructor<?> > m_OptionNames;
	
	static FactoryDhcpOption		s_Instance;
}
