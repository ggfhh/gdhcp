package de.ghoe.guenter.tools.net.udp.dhcp.options;

import de.ghoe.guenter.tools.net.udp.dhcp.options.base.DhcpOptionInts;
import de.ghoe.guenter.tools.net.udp.dhcp.options.base.OptionParseInfo;

public class DhcpOptionLogServer extends DhcpOptionInts
{
	public DhcpOptionLogServer( int[] param)
	{
		super( TYPE_ID, NAME_ID, param);
	}
	
	public DhcpOptionLogServer( OptionParseInfo info) throws Exception
	{
		super(TYPE_ID, NAME_ID, info);
	}
	
	public DhcpOptionLogServer( String in) throws Exception
	{
		super( TYPE_ID, NAME_ID, in);
	}

	public static final Byte TYPE_ID										= 7;
	public static final String NAME_ID										= "LogServer";
}
