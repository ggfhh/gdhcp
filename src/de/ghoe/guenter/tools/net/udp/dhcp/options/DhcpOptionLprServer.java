package de.ghoe.guenter.tools.net.udp.dhcp.options;

import de.ghoe.guenter.tools.net.udp.dhcp.options.base.DhcpOptionInts;
import de.ghoe.guenter.tools.net.udp.dhcp.options.base.OptionParseInfo;

public class DhcpOptionLprServer extends DhcpOptionInts
{
	public DhcpOptionLprServer( int[] param)
	{
		super( TYPE_ID, NAME_ID, param);
	}
	
	public DhcpOptionLprServer( OptionParseInfo info) throws Exception
	{
		super(TYPE_ID, NAME_ID, info);
	}
	
	public DhcpOptionLprServer( String in) throws Exception
	{
		super( TYPE_ID, NAME_ID, in);
	}

	public static final Byte TYPE_ID										= 9;
	public static final String NAME_ID										= "LprServer";
}
