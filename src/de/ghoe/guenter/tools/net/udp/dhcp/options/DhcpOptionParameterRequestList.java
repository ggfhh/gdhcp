package de.ghoe.guenter.tools.net.udp.dhcp.options;

import de.ghoe.guenter.tools.net.udp.dhcp.options.base.DhcpOptionBytes;

public class DhcpOptionParameterRequestList extends DhcpOptionBytes
{
	public DhcpOptionParameterRequestList( byte[] data, int off, int len)
	{
		super( TYPE_ID, NAME_ID, data, off+2, data[off+1]);		
	}

	public static final Byte TYPE_ID										= 55;
	public static final String NAME_ID										= "ParameterRequestList";
}
