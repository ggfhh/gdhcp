package de.ghoe.guenter.tools.net.udp.dhcp.options.base;

import java.util.ArrayList;

import de.ghoe.guenter.tools.net.udp.dhcp.DhcpInfo;

public class DhcpOptionLongs extends DhcpOption
{
	protected DhcpOptionLongs( byte id, String name, long[] val)
	{
		super( id, name);
		m_Data = val;
	}
	
	protected DhcpOptionLongs( byte id, String name, OptionParseInfo info) throws Exception
	{
		super( id, name);
		m_Data = info.readLongs();
	}
	
	protected DhcpOptionLongs( byte id, String name, String val) throws Exception
	{
		super( id, name);
		
		ArrayList<Long> al = new ArrayList<Long>();		
		StringLongExtractor se = new StringLongExtractor( val);		
		
		while(se.hasValues())
		{
			long v = se.extractLong();
			al.add( Long.valueOf( v));
		}
		m_Data = new long[ al.size()];
		for (int i = 0; i < al.size(); ++i)
		{
			m_Data[i] = al.get(i);
		}
	}
	
	public long getValue( int idx)
	{
		return m_Data[idx];
	}
	
	public long[] getVal( )
	{
		return m_Data;
	}

	@Override
	public byte[] getBytes()
	{
		byte[] b = new byte[m_Data.length * 8 + 2];		
		b[0] = getId();
		b[1] = (byte) (m_Data.length * 8);
		
		int pos = 1;
		for (int i = 0; i < m_Data.length; ++i)
		{
			long d = m_Data[i];
			
			b[++pos] = (byte) (d >> 56);
			b[++pos] = (byte) (d >> 48);
			b[++pos] = (byte) (d >> 40);
			b[++pos] = (byte) (d >> 32);			
			b[++pos] = (byte) (d >> 24);
			b[++pos] = (byte) (d >> 16);
			b[++pos] = (byte) (d >>  8);
			b[++pos] = (byte) (d);			
		}
		return b;
	}

	@Override
	public int getLen()
	{
		int len = 8 * m_Data.length;
		return 2 + len;	/* id + len + data */
	}

	@Override
	public int insertData(byte[] buf, int pos)
	{
		int len = m_Data.length;
		buf[pos++]   = getId();
		buf[pos++] = (byte) len;
		
		for (int i = 0; i < m_Data.length; ++i)
		{
			DhcpInfo.setLong(buf, pos, m_Data[i]);
			pos += 8;
		}
		return len + 2;
	}
		
	private long[] m_Data;
}
