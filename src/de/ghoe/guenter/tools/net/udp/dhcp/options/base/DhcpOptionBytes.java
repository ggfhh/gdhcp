package de.ghoe.guenter.tools.net.udp.dhcp.options.base;

import java.util.ArrayList;

public class DhcpOptionBytes extends DhcpOption
{
	protected DhcpOptionBytes( byte id, String name, byte[] val)
	{
		super( id, name);
		m_Data = val;
	}
	
	protected DhcpOptionBytes( byte id, String name, OptionParseInfo info) throws Exception
	{
		super( id, name);
		m_Data = info.readBytes();		
	}
	
	protected DhcpOptionBytes( byte id, String name, String val) throws Exception
	{
		super( id, name);
		
		ArrayList<Byte> al = new ArrayList<Byte>();
		StringLongExtractor se = new StringLongExtractor( val);		
		
		while(se.hasValues())
		{
			byte v = se.extractByte();
			al.add( Byte.valueOf( v));
		}
		m_Data = new byte[ al.size()];
		for (int i = 0; i < al.size(); ++i)
		{
			m_Data[i] = al.get(i);
		}
	}
	
	protected DhcpOptionBytes( byte id, String name, byte[] data, int off, int len)
	{
		super( id, name);		
		m_Data = new byte[len];
		System.arraycopy(data, off, m_Data, 0, len);		
	}
	
	public byte[] getValue( )
	{
		return m_Data;
	}

	public byte getValue( int idx)
	{
		return m_Data[idx];
	}
	
	@Override
	public byte[] getBytes()
	{
		byte[] b = new byte[m_Data.length + 2];		
		b[0] = getId();
		b[1] = (byte) m_Data.length;
		System.arraycopy( m_Data, 0, b, 2, m_Data.length);
		return b;
	}
	
	@Override
	public int getLen()
	{
		return 2 + m_Data.length;	/* id + len + data */
	}

	@Override
	public int insertData(byte[] buf, int pos)
	{
		int len = m_Data.length;
		buf[pos]   = getId();
		buf[++pos] = (byte) len;
		System.arraycopy( m_Data, 0, buf, ++pos, len);
		return len + 2;
	}
	
	private byte[] m_Data;
}
