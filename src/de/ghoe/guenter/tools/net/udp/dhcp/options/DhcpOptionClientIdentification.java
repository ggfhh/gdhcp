package de.ghoe.guenter.tools.net.udp.dhcp.options;

import de.ghoe.guenter.tools.net.udp.dhcp.options.base.DhcpOptionBytes;
import de.ghoe.guenter.tools.net.udp.dhcp.options.base.OptionParseInfo;

public class DhcpOptionClientIdentification extends DhcpOptionBytes
{
	public DhcpOptionClientIdentification( byte[] param)
	{
		super( TYPE_ID, NAME_ID, param);
	}
	
	public DhcpOptionClientIdentification( OptionParseInfo info) throws Exception
	{
		super(TYPE_ID, NAME_ID, info);
	}

	public DhcpOptionClientIdentification( String in) throws Exception
	{
		super( TYPE_ID, NAME_ID, in);
	}
	
	public static final Byte TYPE_ID										= 61;
	public static final String NAME_ID										= "ClientIdentification";
}
