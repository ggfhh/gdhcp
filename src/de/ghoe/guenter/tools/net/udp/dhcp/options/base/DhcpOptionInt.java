package de.ghoe.guenter.tools.net.udp.dhcp.options.base;

import de.ghoe.guenter.tools.net.udp.dhcp.DhcpInfo;

public class DhcpOptionInt extends DhcpOption
{
	protected DhcpOptionInt( byte id, String name, int val)
	{
		super( id, name);
		m_Data = val;
	}
	
	protected DhcpOptionInt( byte id, String name, OptionParseInfo info) throws Exception
	{
		super( id, name);
		m_Data = info.readInt();
	}
	
	protected DhcpOptionInt( byte id, String name, String val) throws Exception
	{
		super( id, name);
		
		StringLongExtractor se = new StringLongExtractor( val);		
		m_Data = se.extractInt();
	}
	
	public int getValue()
	{
		return m_Data;
	}
	
	@Override
	public byte[] getBytes()
	{
		byte[] b = new byte[6];		
		b[0] = getId();
		b[1] = 4;
		
		b[2] = (byte) (m_Data >> 24);
		b[3] = (byte) (m_Data >> 16);
		b[4] = (byte) (m_Data >>  8);
		b[5] = (byte) (int) (m_Data);
		return b;
	}
	
	@Override
	public int getLen()
	{
		return 6;	/* id + len + data */
	}

	@Override
	public int insertData(byte[] buf, int pos)
	{
		buf[pos]   = getId();
		buf[++pos] = 4;
		DhcpInfo.setInt(buf, ++pos, m_Data);
		return 6;
	}
		
	private Integer m_Data;
}
