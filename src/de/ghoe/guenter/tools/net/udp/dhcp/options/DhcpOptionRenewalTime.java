package de.ghoe.guenter.tools.net.udp.dhcp.options;

import de.ghoe.guenter.tools.net.udp.dhcp.options.base.DhcpOptionInt;
import de.ghoe.guenter.tools.net.udp.dhcp.options.base.OptionParseInfo;

public class DhcpOptionRenewalTime extends DhcpOptionInt
{
	public DhcpOptionRenewalTime( int param)
	{
		super( TYPE_ID, NAME_ID, param);
	}
	
	public DhcpOptionRenewalTime( OptionParseInfo info) throws Exception
	{
		super(TYPE_ID, NAME_ID, info);
	}
	
	public DhcpOptionRenewalTime	( String in) throws Exception
	{
		super( TYPE_ID, NAME_ID, in);
	}

	public static final Byte TYPE_ID										= 58;
	public static final String NAME_ID										= "RenewalTime";
}
