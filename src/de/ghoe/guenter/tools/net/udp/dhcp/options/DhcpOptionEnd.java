package de.ghoe.guenter.tools.net.udp.dhcp.options;

import de.ghoe.guenter.tools.net.udp.dhcp.options.base.DhcpOptionNone;
import de.ghoe.guenter.tools.net.udp.dhcp.options.base.OptionParseInfo;

public class DhcpOptionEnd extends DhcpOptionNone
{
	public DhcpOptionEnd()
	{
		super(TYPE_ID, NAME_ID);
	}
	
	public DhcpOptionEnd( OptionParseInfo info)
	{
		super(TYPE_ID, NAME_ID, info);
	}
	
	public DhcpOptionEnd( String in)
	{
		super( TYPE_ID, NAME_ID);
	}
	
	public boolean lastOption()
	{
		return true;
	}
	
	public static final Byte TYPE_ID										= -1;
	public static final String NAME_ID										= "End";
}
