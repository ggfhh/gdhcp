package de.ghoe.guenter.tools.net.udp.dhcp.options;

import de.ghoe.guenter.tools.net.udp.dhcp.options.base.DhcpOptionInt;
import de.ghoe.guenter.tools.net.udp.dhcp.options.base.OptionParseInfo;

public class DhcpOptionRebindTime extends DhcpOptionInt
{
	public DhcpOptionRebindTime( int param)
	{
		super( TYPE_ID, NAME_ID, param);
	}
	
	public DhcpOptionRebindTime( OptionParseInfo info) throws Exception
	{
		super(TYPE_ID, NAME_ID, info);
	}
	
	public DhcpOptionRebindTime( String in) throws Exception
	{
		super( TYPE_ID, NAME_ID, in);
	}

	public static final Byte TYPE_ID										= 59;
	public static final String NAME_ID										= "RebindTime";
}
