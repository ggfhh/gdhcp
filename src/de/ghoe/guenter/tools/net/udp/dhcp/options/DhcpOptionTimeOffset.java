package de.ghoe.guenter.tools.net.udp.dhcp.options;

import de.ghoe.guenter.tools.net.udp.dhcp.options.base.DhcpOptionInt;
import de.ghoe.guenter.tools.net.udp.dhcp.options.base.OptionParseInfo;

public class DhcpOptionTimeOffset extends DhcpOptionInt
{
	public DhcpOptionTimeOffset ( int param)
	{
		super( TYPE_ID, NAME_ID, param);
	}
	
	public DhcpOptionTimeOffset ( OptionParseInfo info) throws Exception
	{
		super(TYPE_ID, NAME_ID, info);
	}
	
	public DhcpOptionTimeOffset( String in) throws Exception
	{
		super( TYPE_ID, NAME_ID, in);
	}

	public static final Byte TYPE_ID										= 2;	
	public static final String NAME_ID										= "TimeOffset";
}
