package de.ghoe.guenter.tools.net.udp.dhcp.options;

import de.ghoe.guenter.tools.net.udp.dhcp.options.base.DhcpOptionInts;
import de.ghoe.guenter.tools.net.udp.dhcp.options.base.OptionParseInfo;

public class DhcpOptionCookieServer extends DhcpOptionInts
{
	public DhcpOptionCookieServer( int[] param)
	{
		super( TYPE_ID, NAME_ID, param);
	}
	
	public DhcpOptionCookieServer( OptionParseInfo info) throws Exception
	{
		super(TYPE_ID, NAME_ID, info);
	}

	public DhcpOptionCookieServer( String in) throws Exception
	{
		super( TYPE_ID, NAME_ID, in);
	}
	
	public static final Byte TYPE_ID										= 8;
	public static final String NAME_ID										= "CookieServer";
}
