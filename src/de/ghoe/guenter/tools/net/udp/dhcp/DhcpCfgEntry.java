package de.ghoe.guenter.tools.net.udp.dhcp;

import de.ghoe.guenter.tools.net.udp.dhcp.options.base.DhcpOptionList;

public class DhcpCfgEntry
{
	public DhcpCfgEntry( int ip4Addr, DhcpCfgList lst)
	{
		m_CfgList = lst;
		m_Ip4Addr = ip4Addr;
		m_LeaseEndTimeMs = 0;	// entry is available
		m_MacAddr = 0;			// no mac assigned
	}
	
	public void free()
	{
		m_LeaseEndTimeMs = System.currentTimeMillis()-1;
		m_MacAddr = 0;
	}
	
	public int getIp4Addr()
	{
		return m_Ip4Addr;		
	}
	
	public String getIp4AddrString()
	{
		return String.format( "%d.%d.%d.%d", ps_getByte( m_Ip4Addr, 3), ps_getByte( m_Ip4Addr, 2), ps_getByte( m_Ip4Addr, 1), ps_getByte( m_Ip4Addr, 0));		
	}
	
	public void setMac( long mac)
	{
		m_MacAddr = mac;
	}
	
	public long getMac()
	{
		return m_MacAddr;
	}
	
	public String getMacString()
	{
		return String.format( "%02x:%02x:%02x:%02x:%02x:%02x",ps_getByte( m_MacAddr,5),ps_getByte(m_MacAddr,4),ps_getByte(m_MacAddr,3),ps_getByte(m_MacAddr,2),ps_getByte(m_MacAddr,1),ps_getByte(m_MacAddr,0));
	}
	
	public void updateEndTime( long leaseTimeMs)
	{
		m_LeaseEndTimeMs = System.currentTimeMillis() + leaseTimeMs;		
	}
	
	public void updateEndTime( )
	{
		updateEndTime( m_CfgList.getLeaseTime());
	}
	
	public String getFilename()
	{
		return m_CfgList.getFilename();
	}
	
	public boolean isMac( long mac)
	{
		return m_MacAddr == mac;
	}
	
	public boolean isActive()
	{
		return m_LeaseEndTimeMs >= System.currentTimeMillis();
	}
	
	public DhcpOptionList getOptions()
	{
		return m_CfgList.getOptions();
	}

	private static int ps_getByte( long val, int idx)
	{
		int v = (int) (val >> (idx * 8));
		v &= 0xff;
		return v;
	}
	
	private DhcpCfgList	m_CfgList;
	private long		m_LeaseEndTimeMs;
	private long		m_MacAddr;
	private int			m_Ip4Addr;
}
