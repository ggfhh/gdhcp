package de.ghoe.guenter.tools.net.udp.dhcp.options;

import de.ghoe.guenter.tools.net.udp.dhcp.options.base.DhcpOptionInts;
import de.ghoe.guenter.tools.net.udp.dhcp.options.base.OptionParseInfo;

public class DhcpOptionImpressServer extends DhcpOptionInts
{
	public DhcpOptionImpressServer( int[] param)
	{
		super( TYPE_ID, NAME_ID, param);
	}
	
	public DhcpOptionImpressServer( OptionParseInfo info) throws Exception
	{
		super(TYPE_ID, NAME_ID, info);
	}
	
	public DhcpOptionImpressServer( String in) throws Exception
	{
		super( TYPE_ID, NAME_ID, in);
	}

	public static final Byte TYPE_ID										= 10;
	public static final String NAME_ID										= "ImpressServer";
}
