package de.ghoe.guenter.tools.net.udp.dhcp.options.base;

public class DhcpOptionString extends DhcpOption
{
	protected DhcpOptionString( byte id, String name, String val)
	{
		super( id, name);
		m_Data = val;
	}
	
	protected DhcpOptionString( byte id, String name, OptionParseInfo info) throws Exception
	{
		super( id, name);
		
		m_Data = info.readString();
	}
	
	public String getValue()
	{
		return m_Data;
	}

	@Override
	public byte[] getBytes()
	{
		byte[] d = m_Data.getBytes();
		
		byte[] b = new byte[ d.length + 2];		
		b[0] = getId();
		b[1] = (byte) (d.length);
		System.arraycopy( d,0, b, 2, d.length);
		
		return b;
	}
	
	@Override
	public int getLen()
	{
		byte[] data = m_Data.getBytes();
		
		return 	data.length + 2;
	}

	@Override
	public int insertData(byte[] buf, int pos)
	{
		byte[] data = m_Data.getBytes();
		int len = data.length;
		buf[pos]   = getId();
		buf[++pos] = (byte) len;
		System.arraycopy( data, 0, buf, ++pos, len);
		return len + 2;
	}
	
	private String m_Data;
}
