package de.ghoe.guenter.tools.net.udp.dhcp.options;

import de.ghoe.guenter.tools.net.udp.dhcp.options.base.DhcpOptionInt;
import de.ghoe.guenter.tools.net.udp.dhcp.options.base.OptionParseInfo;

public class DhcpOptionBroadcastAddress extends DhcpOptionInt
{
	public DhcpOptionBroadcastAddress( int param)
	{
		super( TYPE_ID, NAME_ID, param);
	}
	
	public DhcpOptionBroadcastAddress( OptionParseInfo info) throws Exception
	{
		super(TYPE_ID, NAME_ID, info);
	}
	
	public DhcpOptionBroadcastAddress( String in) throws Exception
	{
		super( TYPE_ID, NAME_ID, in);
	}

	public static final Byte TYPE_ID										= 28;
	public static final String NAME_ID										= "BroadcastAddress";
}
