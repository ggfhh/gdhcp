package de.ghoe.guenter.tools.net.udp.dhcp;

import java.util.ArrayList;
import java.util.Iterator;

import de.ghoe.guenter.support.Log;
import de.ghoe.guenter.tool.dhcp.MacConfig;
import de.ghoe.guenter.tools.net.support.MacUtil;
import de.ghoe.guenter.tools.net.udp.dhcp.options.DhcpOptionIpLeaseTime;
import de.ghoe.guenter.tools.net.udp.dhcp.options.base.DhcpOption;
import de.ghoe.guenter.tools.net.udp.dhcp.options.base.DhcpOptionList;

public class DhcpCfgList
{
	public DhcpCfgList( int ip4Start, int ip4End, ArrayList<MacConfig> maclist, long leaseTime)
	{
		p_init( ip4Start, ip4End, maclist, leaseTime);
	}
	
	public DhcpCfgList( int ip4Start, int ip4End, ArrayList<MacConfig> maclist)
	{
		p_init( ip4Start, ip4End, maclist, DEFAULT_LEASE_TIME_MS);
	}
	
	public DhcpCfgList( int ip4Start, int ip4End, long leaseTime)
	{
		p_init( ip4Start, ip4End, null, leaseTime);
	}
	
	public DhcpCfgList( int ip4Start, int ip4End)
	{
		p_init( ip4Start, ip4End, null, DEFAULT_LEASE_TIME_MS);
	}
	
	public DhcpCfgEntry getNewEntry( long mac)
	{
		DhcpCfgEntry res = getKnownEntry( mac);
		if (null == res)
		{	// now look for a new entry
			synchronized( m_EntryList)
			{
				// first check if this mac is already known
				for (Iterator<DhcpCfgEntry> it = m_EntryList.iterator(); it.hasNext();)
				{
					DhcpCfgEntry e = it.next();
					if (!e.isActive())
					{	// got a new entry
						e.setMac( mac);
						e.updateEndTime();
						res = e;
						Log.logDebug( "Created new Mac-Entry for  " + MacUtil.s_getMacString(mac));
						break;
					}
				}				
			}					
		}
		else
		{
			Log.logDebug( "Mac " + MacUtil.s_getMacString(mac) + " already known");
		}
		return res;
	}
	
	public DhcpCfgEntry getKnownEntry( long mac)
	{
		DhcpCfgEntry res = null;
		
		if (isResponsible(mac))
		{	// list is responsible for this mac
			synchronized( m_EntryList)
			{
				// first check if this mac is already known
				for (Iterator<DhcpCfgEntry> it = m_EntryList.iterator(); it.hasNext();)
				{
					DhcpCfgEntry e = it.next();
					if (e.isMac(mac))
					{
						e.updateEndTime();
						res = e;
						break;
					}
				}				
			}		
		}
		return res;		
	}
	
	public String getFilename()
	{
		return m_FileName;
	}
	
	public void setLeaseTime( long ltMs)
	{
		DhcpOptionIpLeaseTime opt = new DhcpOptionIpLeaseTime( (int) (ltMs / 1000));
		p_setLeaseTime( opt);
	}
	
	public long getLeaseTime()
	{
		return m_LeaseTimeMs;
	}
	
	public void addEntry( DhcpCfgEntry e)
	{
		m_EntryList.add( e);		
	}
	
	public DhcpCfgEntry getIp4Entry( long ip)
	{
		for (Iterator<DhcpCfgEntry> it = m_EntryList.iterator(); it.hasNext();)
		{
			DhcpCfgEntry e = it.next();
			if (e.getIp4Addr() == ip) return e;			
		}
		return null;
	}
	
	public void addOption( DhcpOption opt)
	{
		if (DhcpOptionIpLeaseTime.class.isInstance(opt))
		{
			p_setLeaseTime( (DhcpOptionIpLeaseTime) opt);
		}
		else
		{
			m_Options.addTail( opt);
		}
	}
	
	public boolean isResponsible( long mac)
	{
		if (null != m_Macs)
		{
			for (MacConfig m : m_Macs)
			{
				if ((m.getStart() <= mac) && (m.getEnd() >= mac))
					return true;
			}
		}
		return false;
	}
	
	public DhcpOptionList getOptions()
	{
		return m_Options;
	}
	
	public void invalidateExistingEntries( DhcpClientInfo client)
	{
		long mac = client.getMac();
		
		synchronized( m_EntryList)
		{
			for (Iterator<DhcpCfgEntry> it = m_EntryList.iterator(); it.hasNext();)
			{
				DhcpCfgEntry e = it.next();
				if (e.isMac( mac))
				{
					e.free();
				}
			}
		}
	}

	
	private void p_init( int ip4Start, int ip4End, ArrayList<MacConfig> maclist, long leaseTime)
	{
		m_LeaseTimeMs = leaseTime;
		m_FileName = null;
		
		m_Macs = maclist;

		m_Options = new DhcpOptionList();		
		m_EntryList = new ArrayList<DhcpCfgEntry>();
		while( ip4Start <= ip4End)
		{
			DhcpCfgEntry e = new DhcpCfgEntry( ip4Start++, this);
			m_EntryList.add( e);
		}
	}
	
	private void p_setLeaseTime( DhcpOptionIpLeaseTime opt)
	{
		m_LeaseTimeMs = opt.getValue();
		m_LeaseTimeMs *= 1000;
		
		m_Options.addTail( opt);
	}

	private ArrayList<MacConfig>		m_Macs;
	
	private long 						m_LeaseTimeMs;
	private DhcpOptionList				m_Options;

	private String						m_FileName;
	private ArrayList<DhcpCfgEntry>		m_EntryList;
	
	public  static final long			DEFAULT_LEASE_TIME_MS			= 7*24*3600*1000;	// default = 7 day's 
}
