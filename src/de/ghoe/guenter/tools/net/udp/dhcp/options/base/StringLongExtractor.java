package de.ghoe.guenter.tools.net.udp.dhcp.options.base;

public class StringLongExtractor
{

	public StringLongExtractor( String in, int off)
	{
		m_Str = in;
		m_Pos = off;
		m_State = IntState.IntState_None;
	}
	
	public StringLongExtractor( String in)
	{
		m_Str = in;
		m_Pos = 0;
		m_State = IntState.IntState_None;
	}
	
	public long extractLong( ) throws Exception
	{
		switch( m_State)
		{
		case IntState_None:	// need to detect type
			skippSpaces();
			m_State = s_getIntType( m_Str, m_Pos);
			return extractLong();
		case IntState_Oct:
			return getOct();
		case IntState_Dec:
			return getDec( );
		case IntState_Hex:
			return getHex( );
		case IntState_Ip4:
			return getIp4( );
		default:
			throw new Exception( "Failed to read value from string");
		}
	}
	
	public boolean hasValues() 
	{
		skippSpaces();
		return (m_Pos < m_Str.length());
	}

	public int extractInt( ) throws Exception
	{
		long val = extractLong();
		if (0 > val) throw new Exception( "Negative values not supported by int");
		if (0x100000000l <= val) throw new Exception( "Value to large for a int");
		
		return (int) val;
	}

	public short extractShort( ) throws Exception
	{
		long val = extractLong();
		if (0 > val) throw new Exception( "Negative values not supported by short");
		if (0x10000 <= val) throw new Exception( "Value to large for a short");
		
		return (short) val;
	}

	public byte extractByte( ) throws Exception
	{
		long val = extractLong();
		if (0 > val) throw new Exception( "Negative values not supported by byte");
		if (0x100 <= val) throw new Exception( "Value to large for a byte");
		
		return (byte) val;
	}
	
	private void skippSpaces()
	{
		while( (m_Pos < m_Str.length()) && Character.isWhitespace( m_Str.charAt(m_Pos)))
		{
			++m_Pos;
		}
	}
	
	private static IntState s_getIntType( String in, int off)
	{		
		if (s_isIp4( in, off))
		{
			return IntState.IntState_Ip4;
		}
		char z = in.charAt(off);
		if ('0' == z)
		{
			z = in.charAt(off+1);
			if ('x' == Character.toLowerCase( z))
			{
				off += 2;
				return IntState.IntState_Hex;
			}
			return IntState.IntState_Oct;
		}
		else if (Character.isDigit( z))
		{
			return IntState.IntState_Dec;
		}
		return IntState.IntState_Invalid;
	}
	
	private static boolean s_isIp4( String in, int off)
	{
		String t1 = in.substring( off);
		String t2 = t1.replaceFirst( "\\s.*","");
		return t2.matches( "\\d+\\.\\d+\\.\\d+\\.\\d+");
	}
	
	private long getOct()
	{
		long v = 0l;
		m_State = IntState.IntState_Invalid;
		while( m_Pos < m_Str.length())
		{
			char z = m_Str.charAt( m_Pos);
			if (('0' > z) || ('7' < z)) break;
			v *= 010;
			v += z - '0';
			++m_Pos;
			m_State = IntState.IntState_None;
		}
		return v;
	}
	
	private long getDec()
	{
		long v = 0l;
		m_State = IntState.IntState_Invalid;
		while( m_Pos < m_Str.length())
		{
			char z = m_Str.charAt( m_Pos);
			if (!Character.isDigit( z)) break;
			v *= 10;
			v += z - '0';
			++m_Pos;
			m_State = IntState.IntState_None;
		}
		return v;		
	}
	
	private long getHex()
	{
		long v = 0l;
		m_State = IntState.IntState_Invalid;
		while( m_Pos < m_Str.length())
		{
			char z = m_Str.charAt( m_Pos);
			if (Character.isDigit( z))
			{
				v *= 0x10;
				v += z - '0';				
			}
			else
			{
				char y = Character.toLowerCase(z);
				if (('a' > y) || ('f' < y)) break;
				v *= 0x10;
				v += y - 'a' + 10;
			}
			++m_Pos;
			m_State = IntState.IntState_None;
		}
		return v;		
	}
	
	private long getIp4() throws Exception
	{
		long val = getIp4Byte();
		if ((IntState.IntState_Ip4 == m_State) && ('.' == m_Str.charAt( m_Pos)))
		{
			++m_Pos;
			val <<= 8;
			val  += getIp4Byte();
			if ((IntState.IntState_Ip4 == m_State) && ('.' == m_Str.charAt( m_Pos)))
			{
				++m_Pos;
				val <<= 8;
				val  += getIp4Byte();
				if ((IntState.IntState_Ip4 == m_State) && ('.' == m_Str.charAt( m_Pos)))
				{
					++m_Pos;
					val <<= 8;
					val  += getIp4Byte();
					m_State = IntState.IntState_None;
					return val;
				}
			}
		}
		m_State = IntState.IntState_Invalid;
		throw new Exception( "Input is not an ip4 value");
	}
	
	private int getIp4Byte() throws Exception
	{
		int v = 0;
		m_State = IntState.IntState_Invalid;

		while( m_Pos < m_Str.length())
		{
			char z = m_Str.charAt(m_Pos);
			if (!Character.isDigit( z)) break;
			v *= 10;
			v += (z - '0');			
			m_State = IntState.IntState_Ip4;
			++m_Pos;
		}
		if (256 <= v) throw new Exception( "Byte value of ip4 is out of range");
		return v;
	}

	
/*
 * 
 * 
 * 	protected static byte[] s_getBytes( String in)
	{
		ArrayList<Byte> al = new ArrayList<Byte>();
		Byte v=0;
		Integer pos=0;
		
		for ( IntState state = s_getInt( in, pos, IntState.IntState_None, v);
			  IntState.IntState_Invalid != state; 
			  state = s_getInt( in, pos, IntState.IntState_None, v))
		{
			al.add( v);
		}
		byte[] b = new byte[al.size()];
		for (int i = 0; i < b.length; ++i)
		{
			b[i] = al.get(i);
		}
		return b;
	}
	
	protected static short[] s_getShorts( String in) 
	{
		ArrayList<Short> al = new ArrayList<Short>();
		Short v=0;
		Integer pos=0;
		
		for ( IntState state = s_getInt( in, pos, IntState.IntState_None, v);
			  IntState.IntState_Invalid != state; 
			  state = s_getInt( in, pos, IntState.IntState_None, v))
		{
			al.add( v);
		}
		short[] b = new short[al.size()];
		for (int i = 0; i < b.length; ++i)
		{
			b[i] = al.get(i);
		}
		return b;
	}
	
	protected static int[] s_getInts( String in)
	{
		ArrayList<Integer> al = new ArrayList<Integer>();
		Integer v=0;
		Integer pos=0;
		
		for ( IntState state = s_getInt( in, pos, IntState.IntState_None, v);
			  IntState.IntState_Invalid != state; 
			  state = s_getInt( in, pos, IntState.IntState_None, v))
		{
			al.add( v);
		}
		int[] b = new int[al.size()];
		for (int i = 0; i < b.length; ++i)
		{
			b[i] = al.get(i);
		}
		return b;
	}
	
	protected static long[] s_getLongs( String in)
	{
		ArrayList<Long> al = new ArrayList<Long>();
		Long v=0l;
		Integer pos=0;
		
		for ( IntState state = s_getInt( in, pos, IntState.IntState_None, v);
			  IntState.IntState_Invalid != state; 
			  state = s_getInt( in, pos, IntState.IntState_None, v))
		{
			al.add( v);
		}
		long[] b = new long[al.size()];
		for (int i = 0; i < b.length; ++i)
		{
			b[i] = al.get(i);
		}
		return b;
	}
	
	protected static IntState s_getInt( StringLongExtractor in, IntState state) 
	{
		Long l = (long) 0;
		state = s_getInt( in, off, MAX_DIGITS_BYTE, state, l);		
		out = (byte) (long) l;
		return state;
	}

	protected static IntState s_getInt( String in, Integer off, IntState state, Short out)
	{
		Long l = (long) 0;
		state = s_getInt( in, off, MAX_DIGITS_SHORT, state, l);		
		out = (short) (long) l;
		return state;
	}
	
	protected static IntState s_getInt( String in, Integer off, IntState state, Integer out)
	{
		Long l = (long) 0;
		state = s_getInt( in, off, MAX_DIGITS_INT, state, l);		
		out = (int) (long) l;
		return state;
	}
	
	protected static IntState s_getInt( String in, Integer off, IntState state, Long out)
	{
		return s_getInt( in, off, MAX_DIGITS_LONG, state, out);
	}
	
	private static IntState s_getInt( String in, Integer off, int[] digits, IntState state, Long out)
	{
		switch( state)
		{

		}
	}
	

 * 		

	
	private static IntState s_getOct( String in, Integer off, int maxDigit, Long out)
	{
		s_skippSpaces( in, off);
		
		long res = 0;		
		while( 0 < maxDigit--)
		{
			char z = in.charAt( off);
			if (('0' > z) || ('7' < z))
			{
				out = res;
				return IntState.IntState_Invalid;
			}
			res *= 010;
			res += z - '0';
		}
		out = res;
		return IntState.IntState_Oct;
	}
	
	private static IntState s_getDec( String in, Integer off, int maxDigit, Long out)
	{
		s_skippSpaces( in, off);
		
		long res = 0;		
		while( 0 < maxDigit--)
		{
			char z = in.charAt( off);
			if (!Character.isDigit( z))
			{
				out = res;
				return IntState.IntState_Invalid;
			}
			res *= 10;
			res += z - '0';
		}
		out = res;
		return IntState.IntState_Dec;
	}
	
	private static IntState s_getHex( String in, Integer off, int maxDigit, Long out)
	{
		s_skippSpaces( in, off);
		
		long res = 0;		
		while( 0 < maxDigit--)
		{
			char z = in.charAt( off);
			
			if (!s_isXDigit( z))
			{
				return IntState.IntState_Invalid;
			}
			res *= 0x10;
			res += s_getHexDigit( z);
		}
		out = res;
		return IntState.IntState_Dec;
	}
	
	private static void s_skippSpaces( String in, Integer off)
	{
		while( Character.isWhitespace( in.charAt( off)))
		{
			++off;
		}
	}
	
	private static boolean s_isXDigit( char z)
	{
		if (Character.isDigit(z))
			return true;
		z = Character.toLowerCase( z);
		return ((z >= 'a')&&(z <= 'f'));
	}
	
	private static int s_getHexDigit( char c)
	{
		if (Character.isDigit(c))
		{
			return c - '0';
		}
		c = Character.toLowerCase(c);
		if (('a' <= c) && ('f' >= c))
		{
			return c - 'a' + 10;
		}
		return -1;
	}
 */
	
	private enum IntState { IntState_Invalid, IntState_None, IntState_Oct, IntState_Dec, IntState_Hex, IntState_Ip4};

	private String  	m_Str;
	private int	    	m_Pos;
	private IntState	m_State;
}
