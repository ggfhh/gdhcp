package de.ghoe.guenter.tools.net.udp.dhcp.options;

import de.ghoe.guenter.tools.net.udp.dhcp.options.base.DhcpOptionInts;
import de.ghoe.guenter.tools.net.udp.dhcp.options.base.OptionParseInfo;

public class DhcpOptionResourceLocationServer extends DhcpOptionInts
{
	public DhcpOptionResourceLocationServer( int[] param)
	{
		super( TYPE_ID, NAME_ID, param);
	}
	
	public DhcpOptionResourceLocationServer( OptionParseInfo info) throws Exception
	{
		super(TYPE_ID, NAME_ID, info);
	}
	
	public DhcpOptionResourceLocationServer( String in) throws Exception
	{
		super( TYPE_ID, NAME_ID, in);
	}

	public static final Byte TYPE_ID										= 11;
	public static final String NAME_ID										= "ResourceLocationServer";
}
