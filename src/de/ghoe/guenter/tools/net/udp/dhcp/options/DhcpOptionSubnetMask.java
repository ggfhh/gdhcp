package de.ghoe.guenter.tools.net.udp.dhcp.options;

import de.ghoe.guenter.tools.net.udp.dhcp.options.base.DhcpOptionInt;
import de.ghoe.guenter.tools.net.udp.dhcp.options.base.OptionParseInfo;

public class DhcpOptionSubnetMask extends DhcpOptionInt
{
	public DhcpOptionSubnetMask( int param)
	{
		super( TYPE_ID, NAME_ID, param);
	}
	
	public DhcpOptionSubnetMask( OptionParseInfo info) throws Exception
	{
		super(TYPE_ID, NAME_ID, info);
	}
	
	public DhcpOptionSubnetMask( String in) throws Exception
	{
		super( TYPE_ID, NAME_ID, in);
	}

	public static final Byte TYPE_ID										= 1;	
	public static final String NAME_ID										= "SubnetMask";
}
