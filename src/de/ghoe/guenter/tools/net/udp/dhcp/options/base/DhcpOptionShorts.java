package de.ghoe.guenter.tools.net.udp.dhcp.options.base;

import java.util.ArrayList;

import de.ghoe.guenter.tools.net.udp.dhcp.DhcpInfo;

public class DhcpOptionShorts extends DhcpOption
{
	protected DhcpOptionShorts( byte id, String name, short[] val)
	{
		super( id, name);
		m_Data = val;
	}
	
	protected DhcpOptionShorts( byte id, String name, OptionParseInfo info) throws Exception
	{
		super( id, name);
		m_Data = info.readShorts();
	}
	
	protected DhcpOptionShorts( byte id, String name, String val) throws Exception
	{
		super( id, name);
		
		ArrayList<Short> al = new ArrayList<Short>();		
		StringLongExtractor se = new StringLongExtractor( val);		
		
		while(se.hasValues())
		{
			short v = se.extractShort();
			al.add( Short.valueOf( v));
		}
		m_Data = new short[ al.size()];
		for (int i = 0; i < al.size(); ++i)
		{
			m_Data[i] = al.get(i);
		}
	}
	
	public short getValue( int idx)
	{		
		return m_Data[idx];
	}
	
	public short[] getVal( )
	{
		return m_Data;
	}
		
	@Override
	public byte[] getBytes()
	{
		byte[] b = new byte[m_Data.length * 2 + 2];		
		b[0] = getId();
		b[1] = (byte) (m_Data.length * 2);
		
		int pos = 1;
		for (int i = 0; i < m_Data.length; ++i)
		{
			short d = m_Data[i];
			
			b[++pos] = (byte) (d >>  8);
			b[++pos] = (byte) (d);			
		}
		return b;
	}

	@Override
	public int getLen()
	{
		int len = 2 * m_Data.length;
		return 2 + len;	/* id + len + data */
	}

	@Override
	public int insertData(byte[] buf, int pos)
	{
		int len = m_Data.length;
		buf[pos++]   = getId();
		buf[pos++] = (byte) len;
		
		for (int i = 0; i < m_Data.length; ++i)
		{
			DhcpInfo.setShort(buf, pos, m_Data[i]);
			pos += 2;
		}
		return len + 2;
	}

	private short[] m_Data;
}
