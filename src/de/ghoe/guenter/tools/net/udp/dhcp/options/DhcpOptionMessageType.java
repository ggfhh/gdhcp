package de.ghoe.guenter.tools.net.udp.dhcp.options;

import de.ghoe.guenter.tools.net.udp.dhcp.options.base.DhcpOptionByte;
import de.ghoe.guenter.tools.net.udp.dhcp.options.base.OptionParseInfo;

public class DhcpOptionMessageType extends DhcpOptionByte
{
	public DhcpOptionMessageType( Type t) throws Exception
	{
		super( TYPE_ID, NAME_ID, s_getByte( t));
	}
	
	public DhcpOptionMessageType( OptionParseInfo info) throws Exception
	{
		super(TYPE_ID, NAME_ID, info);
	}
	
	public DhcpOptionMessageType( String in) throws Exception
	{
		super( TYPE_ID, NAME_ID, s_getTypeByte( in));
	}

	public enum Type
	{
		DHCPDISCOVER, DHCPOFFER, DHCPREQUEST, DHCPDECLINE, DHCPACK, DHCPNAK, DHCPRELEASE, DHCPINFORM
	}
	
	public Type getType( ) throws Exception
	{
		byte b = getValue();
		Type t = s_getType( b);
		return t;
	}
	
	public String getTypeString()
	{
		byte b = getValue();
		return TYPE_NAMES[b];
	}
	
	private static byte s_getByte( Type type) throws Exception
	{
		switch( type)
		{
		case DHCPDISCOVER:
			return (byte) 1;
		case DHCPOFFER:
			return (byte) 2;
		case DHCPREQUEST:
			return (byte) 3;
		case DHCPDECLINE:
			return (byte) 4;
		case DHCPACK:
			return (byte) 5;
		case DHCPNAK:
			return (byte) 6;
		case DHCPRELEASE:
			return (byte) 7;
		case DHCPINFORM:
			return (byte) 8;
		}
		throw new Exception( "Unknown type provided!");
	}
	
	private static Type s_getType( byte b) throws Exception
	{
		if ((1 > b) || (TYPES.length + 1 < b))
		{
			throw new Exception( "Type " + s_getShort( b) + " is invalid (only 1..8)");
		}
		return TYPES[b-1];
	}
	
//	private static Type s_getType( String in) throws Exception
//	{
//		byte b = s_getTypeByte( in);
//		Type t = s_getType( b);
//		return t;
//	}
	
	private static byte s_getTypeByte( String in) throws Exception
	{
		String cmp = in.toUpperCase();
		
		for (int i = 0; i < TYPE_NAMES.length; ++i)
		{
			if (TYPE_NAMES[i].equals( cmp))
				return (byte) (i+1);
		}
		throw new Exception( "Unknown dhcp-type selected: " + in);
	}
	
	public static final Byte TYPE_ID			= 53;
	public static final String NAME_ID			= "MessageType";

	private static final Type[]  TYPES			= { Type.DHCPDISCOVER, Type.DHCPOFFER, Type.DHCPREQUEST, Type.DHCPDECLINE, Type.DHCPACK, Type.DHCPNAK, Type.DHCPRELEASE, Type.DHCPINFORM};
	private static final String[]  TYPE_NAMES	= { "DHCPDISCOVER", "DHCPOFFER", "DHCPREQUEST", "DHCPDECLINE", "DHCPACK", "DHCPNAK", "DHCPRELEASE", "DHCPINFORM"};
}
