package de.ghoe.guenter.tools.net.udp.dhcp.options;

import de.ghoe.guenter.tools.net.udp.dhcp.options.base.DhcpOptionString;
import de.ghoe.guenter.tools.net.udp.dhcp.options.base.OptionParseInfo;

public class DhcpOptionMeritDumpFile extends DhcpOptionString
{
	public DhcpOptionMeritDumpFile( String param)
	{
		super( TYPE_ID, NAME_ID, param);
	}
	
	public DhcpOptionMeritDumpFile( OptionParseInfo info) throws Exception
	{
		super(TYPE_ID, NAME_ID, info);
	}

	public static final Byte TYPE_ID										= 14;
	public static final String NAME_ID										= "MeritDumpFile";
}
