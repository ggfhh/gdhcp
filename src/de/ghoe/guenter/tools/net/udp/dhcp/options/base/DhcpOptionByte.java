package de.ghoe.guenter.tools.net.udp.dhcp.options.base;

public class DhcpOptionByte extends DhcpOption
{
	protected DhcpOptionByte( byte id, String name, byte val)
	{
		super( id, name);
		m_Data = val;
	}
	
	protected DhcpOptionByte( byte id, String name, OptionParseInfo info) throws Exception
	{
		super( id, name);
		m_Data = info.readByte();
	}
	
	protected DhcpOptionByte( byte id, String name, String val) throws Exception
	{
		super( id, name);
	
		StringLongExtractor se = new StringLongExtractor( val);		
		m_Data = se.extractByte();
	}
		
	public byte getValue()
	{
		return m_Data;
	}
	
	@Override
	public byte[] getBytes()
	{
		byte[] b = new byte[3];		
		b[0] = getId();
		b[1] = 1;
		b[2] = m_Data;
		return b;
	}
	
	@Override
	public int getLen()
	{
		return 3;	/* id + len + data */
	}

	@Override
	public int insertData(byte[] buf, int pos)
	{
		buf[pos]   = getId();
		buf[++pos] = 1;
		buf[++pos] = m_Data; 
		
		return 3;
	}
	
	private Byte m_Data;
}
