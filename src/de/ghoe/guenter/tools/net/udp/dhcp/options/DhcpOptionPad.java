package de.ghoe.guenter.tools.net.udp.dhcp.options;

import de.ghoe.guenter.tools.net.udp.dhcp.options.base.DhcpOptionNone;
import de.ghoe.guenter.tools.net.udp.dhcp.options.base.OptionParseInfo;

public class DhcpOptionPad extends DhcpOptionNone
{
	public DhcpOptionPad()
	{
		super( TYPE_ID, NAME_ID);
	}
	
	public DhcpOptionPad( OptionParseInfo info)
	{
		super(TYPE_ID, NAME_ID, info);
	}
	
	public DhcpOptionPad( String in)
	{
		super( TYPE_ID, NAME_ID);
	}
	
	public static final Byte TYPE_ID										= 0;
	public static final String NAME_ID										= "Pad";
}
