package de.ghoe.guenter.tools.net.udp.dhcp.options;

import de.ghoe.guenter.tools.net.udp.dhcp.options.base.DhcpOptionInt;
import de.ghoe.guenter.tools.net.udp.dhcp.options.base.OptionParseInfo;

public class DhcpOptionServerIdentification extends DhcpOptionInt
{
	public DhcpOptionServerIdentification( int param)
	{
		super( TYPE_ID, NAME_ID, param);
	}
	
	public DhcpOptionServerIdentification( OptionParseInfo info) throws Exception
	{
		super(TYPE_ID, NAME_ID, info);
	}
	
	public DhcpOptionServerIdentification( String in) throws Exception
	{
		super( TYPE_ID, NAME_ID, in);
	}

	public static final Byte TYPE_ID										= 54;
	public static final String NAME_ID										= "ServerIdentification";
}
