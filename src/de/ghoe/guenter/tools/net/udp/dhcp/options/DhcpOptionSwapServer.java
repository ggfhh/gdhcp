package de.ghoe.guenter.tools.net.udp.dhcp.options;

import de.ghoe.guenter.tools.net.udp.dhcp.options.base.DhcpOptionInts;
import de.ghoe.guenter.tools.net.udp.dhcp.options.base.OptionParseInfo;

public class DhcpOptionSwapServer extends DhcpOptionInts
{
	public DhcpOptionSwapServer( int[] param)
	{
		super( TYPE_ID, NAME_ID, param);
	}
	
	public DhcpOptionSwapServer( OptionParseInfo info) throws Exception
	{
		super(TYPE_ID, NAME_ID, info);
	}
	
	public DhcpOptionSwapServer( String in) throws Exception
	{
		super( TYPE_ID, NAME_ID, in);
	}

	public static final Byte TYPE_ID										= 16;
	public static final String NAME_ID										= "SwapServer";
}
