package de.ghoe.guenter.tools.net.udp.dhcp.options.base;

import java.util.ArrayList;
import java.util.Iterator;

import de.ghoe.guenter.support.Log;
import de.ghoe.guenter.tools.net.udp.dhcp.DhcpInfo;
import de.ghoe.guenter.tools.net.udp.dhcp.options.DhcpOptionEnd;

public class DhcpOptionList
{
	public DhcpOptionList( byte[] dhcpPacket, int packetLen)
	{
		m_List = new ArrayList<DhcpOption>();
		
		if (DhcpInfo.DHCP_MIN_PACKET_SIZE < dhcpPacket.length)
		{
			int cookie = DhcpInfo.getInt( dhcpPacket, DhcpInfo.BOOTP_PACKET_SIZE);
			
			if (DhcpInfo.DHCP_MAGIC_COOKIE == cookie)
			{
				FactoryDhcpOption.s_Instance().addOptions( m_List, dhcpPacket, DhcpInfo.DHCP_MIN_PACKET_SIZE, packetLen);
			}
		}		
		getOptions( dhcpPacket, packetLen);
	}
	
	private void getOptions( byte[] data, int dataLen)
	{
		int pos = DhcpInfo.DHCP_POS_OPTIONS;
		while( pos + 1< dataLen)
		{
			DhcpOption o = FactoryDhcpOption.s_Instance.createOption( data, pos, dataLen);
			int len = data[pos+1];
			if (0 > len) len = 0;
			if (null != o)
			{
				Log.logDebug( "Detected option at pos " + pos + '/' + data.length + " with lenght " + len + " --> " + o.toString());
			}
			else
			{
				Log.logDebug( "Detected unknown option at pos " + pos + '/' + data.length + " with lenght " + len);
			}
			pos += len + 2;
		}
	}
	
	public DhcpOptionList( DhcpOptionList old)
	{
		m_List = new ArrayList<DhcpOption>( old.m_List);
	}
	
	public DhcpOptionList( ArrayList<DhcpOption> opts)
	{
		m_List = new ArrayList<DhcpOption>( opts);
	}
	
	public DhcpOptionList( )
	{
		m_List = new ArrayList<DhcpOption>( );
	}
	
	public int getOptionPlainSize()
	{
		int len = 0;
		
		synchronized( m_List)
		{
			for (Iterator<DhcpOption> it = m_List.iterator(); it.hasNext(); )
			{
				DhcpOption option = it.next();
				len += option.getLen();			
			}
		}
		return len;		
	}

	public void put( byte[] out, int pos)
	{
		synchronized( m_List)
		{
			for (Iterator<DhcpOption> it = m_List.iterator(); it.hasNext(); )
			{
				DhcpOption option = it.next();
				pos += option.insertData( out, pos);
			}
		}
	}
	
	public void addHead( DhcpOption opt)
	{
		synchronized( m_List)
		{
			removeExisting( opt.getId());
			m_List.add( 0, opt);
		}
	}
	
	public void addTail( DhcpOption opt)
	{
		synchronized( m_List)
		{
			removeExisting( opt.getId());
			m_List.add( opt);
		}
	}
	
	public void addTail( DhcpOptionList l, byte type)
	{
		synchronized( m_List)
		{
			for (Iterator<DhcpOption> it = l.getOptions().iterator(); it.hasNext(); )
			{
				DhcpOption o = it.next();
				if (o.getId() == type)
				{
					removeExisting( type);
					m_List.add( o);
					break;
				}
			}
		}
	}
	
	public byte[] getOptionPlain()
	{
		int len = getOptionPlainSize();		
		byte[] buf = new byte[len]; 
		
		int pos = 0;
		synchronized( m_List)
		{
			for (Iterator<DhcpOption> it = m_List.iterator(); it.hasNext(); )
			{
				DhcpOption option = it.next();
				
				pos += option.insertData( buf, pos);
			}
		}
		return buf;
	}
	
	public ArrayList<DhcpOption> getOptions()
	{
		ArrayList<DhcpOption> opts;
		synchronized( m_List)
		{
			opts = new ArrayList<DhcpOption>( m_List);			
		}
		// need to terminate option-list
		opts.add( new DhcpOptionEnd());
		return opts;
	}
	
	public DhcpOption get( byte id)
	{
		synchronized( m_List)
		{
			for (Iterator<DhcpOption> it = m_List.iterator(); it.hasNext(); )
			{
				DhcpOption option = it.next();
				
				if (option.getId() == id)
					return option;
			}
		}
		return null;
	}
	
	private void removeExisting( byte id)
	{
		for (int i = 0; i < m_List.size(); ++i)
		{
			DhcpOption o = m_List.get(i);
			if (o.getId() == id)
			{
				m_List.remove( i);
				return;
			}
		}
	}
	
	private ArrayList<DhcpOption>	m_List;
}
