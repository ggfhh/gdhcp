package de.ghoe.guenter.tools.net.udp.dhcp;

import de.ghoe.guenter.tools.net.udp.dhcp.options.base.DhcpOption;
import de.ghoe.guenter.tools.net.udp.dhcp.options.base.DhcpOptionList;

public class DhcpClientInfo
{
	public DhcpClientInfo( byte[] buf, int len)
	{
		m_Type = buf[1];
		m_Id   = DhcpInfo.getInt( buf, DhcpInfo.BOOTP_POS_XID);
		m_CIp  = DhcpInfo.getInt( buf, DhcpInfo.BOOTP_POS_CIADDR);
		m_OIp  = DhcpInfo.getInt( buf, DhcpInfo.BOOTP_POS_YIADDR);
		m_SIp  = DhcpInfo.getInt( buf, DhcpInfo.BOOTP_POS_SIADDR);
		m_GIp  = DhcpInfo.getInt( buf, DhcpInfo.BOOTP_POS_GIADDR);
		
		m_Mac  = DhcpInfo.getLong( buf, DhcpInfo.BOOTP_POS_CHIADDR, DhcpInfo.DHCP_ADDRLEN);
		
		m_Options = new DhcpOptionList( buf, len);
	}
	
	public DhcpOption getOption( byte id)
	{
		DhcpOption opt = m_Options.get( id);
		return opt;
	}
	
	public DhcpOptionList getOptions()
	{
		return m_Options;
	}
	
	public long getMac()
	{
		return m_Mac;
	}
	
	public String getMacString()
	{
		StringBuilder sb = new StringBuilder();
		
		long mac = m_Mac;
		for (int i = 40; 0 < i; i-=8)
		{
			sb.append( getByteString( mac >> i));
			sb.append( ":");
		}
		sb.append( getByteString( mac));
		String res = sb.toString();
		
		return res;
	}
	
	private String getByteString( long v)
	{
		v &= 0xff;
		String s = String.format( "%02x", v);
		return s;
	}
	
	public byte getType()
	{
		return m_Type;
	}
	
	public int getId()
	{
		return m_Id;
	}
	
	public int getClientIp()
	{
		return m_CIp;
	}
	
	public int getOwnIp()
	{
		return m_OIp;
	}
	
	public int getServerIp()
	{
		return m_SIp;
	}
	
	public int getGatewayIp()
	{
		return m_GIp;
	}
	
	public void setIp( int ip)
	{
		m_CIp = ip;
	}
	
	private byte	m_Type;
	private int		m_Id;
	private int		m_CIp;
	private int		m_OIp;
	private int		m_SIp;
	private int		m_GIp;	
	private long	m_Mac;
	
	private DhcpOptionList	m_Options;
	
	public final static int TYPE_ETH		= 1;
	public final static int TYPE_IEEE802	= 6;
}
