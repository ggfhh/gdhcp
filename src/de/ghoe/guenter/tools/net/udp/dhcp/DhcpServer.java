package de.ghoe.guenter.tools.net.udp.dhcp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Iterator;

import de.ghoe.guenter.support.IntfUiClient;
import de.ghoe.guenter.support.IntfUiServer;
import de.ghoe.guenter.support.Log;
import de.ghoe.guenter.tool.dhcp.MacConfig;
import de.ghoe.guenter.tools.net.support.HostIp4Addresses;
import de.ghoe.guenter.tools.net.support.MacUtil;
import de.ghoe.guenter.tools.net.udp.UdpServer;
import de.ghoe.guenter.tools.net.udp.dhcp.options.DhcpOptionClientIdentification;
import de.ghoe.guenter.tools.net.udp.dhcp.options.DhcpOptionDomainName;
import de.ghoe.guenter.tools.net.udp.dhcp.options.DhcpOptionDomainNameServer;
import de.ghoe.guenter.tools.net.udp.dhcp.options.DhcpOptionEnd;
import de.ghoe.guenter.tools.net.udp.dhcp.options.DhcpOptionHostName;
import de.ghoe.guenter.tools.net.udp.dhcp.options.DhcpOptionIpLeaseTime;
import de.ghoe.guenter.tools.net.udp.dhcp.options.DhcpOptionMessageType;
import de.ghoe.guenter.tools.net.udp.dhcp.options.DhcpOptionNtpServer;
import de.ghoe.guenter.tools.net.udp.dhcp.options.DhcpOptionRebindTime;
import de.ghoe.guenter.tools.net.udp.dhcp.options.DhcpOptionRenewalTime;
import de.ghoe.guenter.tools.net.udp.dhcp.options.DhcpOptionRouter;
import de.ghoe.guenter.tools.net.udp.dhcp.options.DhcpOptionServerIdentification;
import de.ghoe.guenter.tools.net.udp.dhcp.options.DhcpOptionSubnetMask;
import de.ghoe.guenter.tools.net.udp.dhcp.options.base.DhcpOption;
import de.ghoe.guenter.tools.net.udp.dhcp.options.base.DhcpOptionList;

public class DhcpServer extends UdpServer
	implements IntfUiClient
{
	public DhcpServer( IntfUiServer ui, long leasTimeMs)
	{
		super( "Dhcp", DEFAULT_SERVER_PORT, DEFAULT_MAX_FRAME_SIZE);
		// System.out.println( "Creating DHCP-Server");
		m_UiServer = ui;
		m_UseLeasTimeMs = leasTimeMs;

		m_DhcpEntries = new ArrayList<DhcpCfgList>();
		
		// get initial configuration
		ui.registerUiClient(this);
		uiUpdateClient();		
	}
	
	public DhcpServer( IntfUiServer ui)
	{
		super( "Dhcp", DEFAULT_SERVER_PORT, DEFAULT_MAX_FRAME_SIZE);		
		// System.out.println( "Creating DHCP-Server");
		m_UiServer = ui;
		m_UseLeasTimeMs = DhcpCfgList.DEFAULT_LEASE_TIME_MS;

		m_DhcpEntries = new ArrayList<DhcpCfgList>();
		
		// get initial configuration
		uiUpdateClient();		
	}
		
	//////////////////////////////////////////////////////////////////////////////////
	// IntfGuiClient 
	//////////////////////////////////////////////////////////////////////////////////	
	@Override
	public boolean uiStartClient()
	{
		boolean res = false;
		try
		{
			super.open();
			res = true;
		}
		catch( Throwable e)
		{
			Log.log( "Failed to start DHCP-Server", e);
		}
		return res;
	}

	@Override
	public void uiStopClient()
	{
		try
		{
			super.shutdown();
		}
		catch( Throwable e)
		{
			Log.log( "Failed to shutdown DHCP-Server", e);			
		}
	}

	@Override
	public boolean uiUpdateClient()
	{
		boolean res = true;

		int i = m_UiServer.getIpStart();
		int e = m_UiServer.getIpEnd();
		ArrayList<MacConfig> macs = m_UiServer.getMacs();

		// 1. change responsible mac's
		synchronized( m_DhcpEntries)
		{
			try
			{
				// currently we have only a single list (use default lease-time)
				DhcpCfgList lst = new DhcpCfgList( i, e, macs, m_UseLeasTimeMs);			
			
				// now check for existent entries
				while( i <= e)
				{
					DhcpCfgEntry entry = p_getEntry( i);
					if (null == entry)
					{
						entry = new DhcpCfgEntry( i, lst);
					}
					lst.addEntry( entry);
					++i;
				}
				// no error -> use new entries 
				m_DhcpEntries.clear();
				m_DhcpEntries.add( lst);				
				res = true;
			}
			catch( Throwable t)
			{
				Log.log( "Failed to update DHCP-Server-Cfg: ", t);
			}
		}
		return res;
	}
	
	@Override
	public boolean uiIsClientStarted()
	{		
		return super.isUdpReceiverUp();
	}
	
	private DhcpCfgEntry p_getEntry( long ip)
	{
		try
		{
			synchronized( m_DhcpEntries)
			{
				for (Iterator<DhcpCfgList> li = m_DhcpEntries.iterator(); li.hasNext();)
				{
					DhcpCfgList lst = li.next();
					DhcpCfgEntry e = lst.getIp4Entry( ip);
					if (null != e)	return e;
				}
			}
		}
		catch( Throwable t)
		{
			System.err.println( "Failed to get DHCP-Server-Ip4Entry: " + t.getLocalizedMessage());			
		}		
		return null;
	}
	
	//////////////////////////////////////////////////////////////////////////////////
	// IntfUdpServer 
	//////////////////////////////////////////////////////////////////////////////////	
	@Override
	public boolean udpRxHandle( DatagramPacket p)
	{
		Log.logVerbose( String.format( "Received packet woth %d bytes from %s", p.getLength(), p.getSocketAddress().toString()));
		DhcpClientInfo client = getDhcpClientInfo( p);
		if (null != client)
		{
			int reqServerIp = client.getServerIp();
			int serverIp    = super.getUsedIpV4();
		
			if ((0 == reqServerIp) || (serverIp == reqServerIp))
			{
				Log.logVerbose( "Found match for ip!");
				
				if (isMacAccepted( client.getMac()))
				{
					DhcpOptionMessageType mt = (DhcpOptionMessageType) client.getOption( DhcpOptionMessageType.TYPE_ID);
					try
					{	
						int port = p.getPort();
						if (null != mt)
						{	/* received dhcp */
							Log.logVerbose( "Got dhcp-request!");
							replyDhcp( client, s_getBroadcastAddress( port), mt);
						}
						else
						{	/* received bootp */
							Log.logVerbose( "Got bootp-request!");
							replyBootP( client, s_getBroadcastAddress( port));
						}
					} 
					catch (Exception e)
					{
						Log.log( "Failed to process operation", e);
					}
				}
			}
			else
			{
				removeExistingEntries( client);
				Log.logEvent( String.format( "DHCP-Request was not form me (srv=0x%x, iam=0x%x)", reqServerIp, serverIp));
			}
		}
		else
		{
			Log.logEvent( "\tClient not found!");
		}
		return true;
	}
	
	//////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////
	private void replyDhcp( DhcpClientInfo client, InetSocketAddress addr, DhcpOptionMessageType mt) throws Exception
	{
		byte[] data = null;
		DhcpOptionMessageType.Type type = mt.getType();
		
		switch( type)
		{
		case DHCPDISCOVER:	// client: new request
			data = getDhcpOffer( client, addr);
			Log.logEvent( "Offer ip to mac " + client.getMacString());
			break;
		case DHCPREQUEST:	// client: requst address
			data = getDhcpAck( client, addr);
			Log.logEvent( "Provide ip to mac " + client.getMacString());
			break;
		case DHCPDECLINE:	// client: reject address
			Log.logEvent( "Decline ip from mac " + client.getMacString());
			removeExistingEntries( client);
			break;
		case DHCPRELEASE:	// client: release address
			Log.logEvent( "Release ip from mac " + client.getMacString());
			removeExistingEntries( client);
			break;
		case DHCPINFORM:	// client: request information (e.g. static ip)
			Log.logEvent( "Inform ip to address " + client.getMacString());
			data = getDhcpAck( client, addr);
		default:
			Log.logWarning( "Unexpected dhcp-reply recived!");
			break;
		}
		if (null != data)
		{
			int port = addr.getPort();
			sendReply( data, s_getBroadcastAddress(port));
		}
	}
	
	private byte[] getDhcpOffer( DhcpClientInfo client, SocketAddress addr) throws Exception
	{
		DhcpCfgEntry e = p_getNewEntryFromMac( client.getMac());
		DhcpOptionMessageType opt = new DhcpOptionMessageType( DhcpOptionMessageType.Type.DHCPOFFER);
		byte[] res = getDhcpReply( client, addr, e, opt);
		return res;
	}
	
	private byte[] getDhcpAck( DhcpClientInfo client, SocketAddress addr) throws Exception
	{
		DhcpCfgEntry e = p_getEntryFromMac( client.getMac());
		DhcpOptionMessageType opt = new DhcpOptionMessageType( DhcpOptionMessageType.Type.DHCPACK);
		byte[] res = getDhcpReply( client, addr, e, opt);
		return res;
	}
	
	private byte[] getDhcpReply( DhcpClientInfo client, SocketAddress addr, DhcpCfgEntry e, DhcpOption opt)
	{
		DhcpOptionList optList = e.getOptions();
		int sip = HostIp4Addresses.s_getBestHostAddress( e.getIp4Addr());
		optList.addHead( new DhcpOptionServerIdentification( sip));
		optList.addHead( opt);
		DhcpOptionList copt = client.getOptions();
		if (Log.s_isLogOn(Log.Level.Debug))
		{
			for ( DhcpOption o : copt.getOptions())
			{
				Log.logDebug( "Add client option: " + o.getId());
			}
		}
		int addrmask = HostIp4Addresses.s_getAddressMask( e.getIp4Addr());
		optList.addTail( new DhcpOptionSubnetMask( addrmask));
		optList.addTail( copt, DhcpOptionClientIdentification.TYPE_ID);
		optList.addTail( copt, DhcpOptionHostName.TYPE_ID);
		optList.addTail( new DhcpOptionIpLeaseTime( DEFAULT_IP4_LEASE_TIME));
		optList.addTail( new DhcpOptionRenewalTime( DEFAULT_IP4_RENEWAL_TIME));
		optList.addTail( new DhcpOptionRebindTime(  DEFAULT_IP4_REBIND_TIME));
		int[] router = new int[1];
		router[0] = sip;
		optList.addTail( new DhcpOptionRouter( router));
		optList.addTail( new DhcpOptionDomainNameServer( router));
		optList.addTail( new DhcpOptionDomainName( "local"));
		optList.addTail( new DhcpOptionNtpServer( router));
		optList.addTail( new DhcpOptionEnd());
		
		int plen = DhcpInfo.DHCP_MIN_PACKET_SIZE + optList.getOptionPlainSize();
		byte[] reply = new byte[plen];
		setBootPReply( reply, client, e, false);
		
		/* now the dhcp data */
		s_setIntVal( reply, DhcpInfo.DHCP_POS_MCOOKI, DhcpInfo.DHCP_MAGIC_COOKIE);
		optList.put( reply, DhcpInfo.DHCP_POS_OPTIONS);
		return reply;
	}
	
	private DhcpClientInfo getDhcpClientInfo( DatagramPacket p)
	{
		if (DhcpInfo.BOOTP_MIN_PACKET_SIZE <= p.getLength())
		{
			byte[] buf = p.getData();
			if ((DhcpInfo.DHCP_REQUEST == buf[0]) && 
				((DhcpClientInfo.TYPE_ETH == buf[1]) || (DhcpClientInfo.TYPE_IEEE802 == buf[1])) &&
			    (DhcpInfo.DHCP_ADDRLEN == buf[2]) &&
			    (DEFAULT_CLIENT_PORT == p.getPort()))
			{
				return new DhcpClientInfo( buf, p.getLength());
			}
			Log.logEvent( "Ignore packet, since invalid data");
		}
		else
		{
			Log.logEvent( "Ignore to smal packet");			
		}
		return null;
	}
	
	private void removeExistingEntries( DhcpClientInfo client)
	{
		for (Iterator<DhcpCfgList> el = m_DhcpEntries.iterator(); el.hasNext();)
		{
			DhcpCfgList l = el.next();			
			l.invalidateExistingEntries( client);
		}
	}
	
	private void replyBootP( DhcpClientInfo client, InetSocketAddress addr) throws Exception
	{
		DhcpCfgEntry e = p_getPrefferedIp( client.getMac(), client.getClientIp());
		if (null != e)
		{
			byte[] buf = createBootPReply( client, e);
			int port = addr.getPort();
			sendReply( buf, s_getBroadcastAddress(port));
		}
	}
	
	private void sendReply( byte[] buf, SocketAddress addr) throws IOException
	{
		Log.logVerbose("Send reply to address " + addr.toString());
		DatagramPacket p = new DatagramPacket( buf, buf.length, addr);
		super.sendPacket( p);
	}

	private void setStringData( byte[] buf, int off, int len, String s)
	{
		if (null != s)
		{
			byte[] data = s.getBytes();
			int hlen = data.length;
			
			if (hlen < len)
			{
				System.arraycopy( data, 0, buf, off, hlen);
				java.util.Arrays.fill( buf, off + hlen, off + len, DhcpInfo.DHCP_FILLBYTE);
			}
			else
			{
				System.arraycopy( data, 0, buf, off, off + len);
			}		
		}
		else
		{
			java.util.Arrays.fill( buf, off, off + len, DhcpInfo.DHCP_FILLBYTE);			
		}
	}
	
	private void setHostName( byte[] buf, int off, int len, boolean setHostname)
	{
		String hostname = null;
		if (setHostname)
		{
			try
			{
				hostname = InetAddress.getLocalHost().getHostName();
			} 
			catch (UnknownHostException e)
			{
			}
		}
		setStringData( buf, off, len, hostname);
	}
	
	private byte[] createBootPReply( DhcpClientInfo client, DhcpCfgEntry e)
	{
		int plen = DhcpInfo.BOOTP_PACKET_SIZE;		
		byte[] reply = new byte[plen];
		
		setBootPReply( reply, client, e, true);
		return reply;
	}
	
	private void setBootPReply( byte[] reply, DhcpClientInfo client, DhcpCfgEntry e, boolean setServerName)
	{
		reply[DhcpInfo.BOOTP_POS_OP] 			= DhcpInfo.DHCP_REPLY;
		reply[DhcpInfo.BOOTP_POS_HTYPE] 		= client.getType();
		reply[DhcpInfo.BOOTP_POS_HLEN] 			= DhcpInfo.DHCP_ADDRLEN;
		reply[DhcpInfo.BOOTP_POS_HOPS] 			= 0;
		s_setIntVal( reply, DhcpInfo.BOOTP_POS_XID, client.getId());
		s_setShortVal( reply, DhcpInfo.BOOTP_POS_SECS, (short) 0);		// client active time
		s_setShortVal( reply, DhcpInfo.BOOTP_POS_FLAGS, (short) 0);		// has already an address
		s_setIntVal( reply, DhcpInfo.BOOTP_POS_CIADDR, 0);
		s_setIntVal( reply, DhcpInfo.BOOTP_POS_YIADDR, e.getIp4Addr());
		int sip = HostIp4Addresses.s_getBestHostAddress( e.getIp4Addr());
//		sip = 0;
		s_setIntVal( reply, DhcpInfo.BOOTP_POS_SIADDR, sip);
		s_setIntVal( reply, DhcpInfo.BOOTP_POS_GIADDR, client.getGatewayIp());
		long clientMac = client.getMac();
		s_setLongVal( reply, DhcpInfo.BOOTP_POS_CHIADDR, DhcpInfo.DHCP_ADDRLEN, clientMac);
		java.util.Arrays.fill( reply, DhcpInfo.BOOTP_POS_CHIADDR + DhcpInfo.DHCP_ADDRLEN, DhcpInfo.BOOTP_POS_SNAME, DhcpInfo.DHCP_FILLBYTE);
		
		setHostName( reply, DhcpInfo.BOOTP_POS_SNAME, DhcpInfo.BOOTP_PACKET_SERVERNAME_LEN, setServerName);
		String fname = e.getFilename();
		setStringData( reply, DhcpInfo.BOOTP_POS_FILE, DhcpInfo.BOOTP_PACKET_FILENAME_LEN, fname);
	}
			
	private static void s_setLongVal( byte[] reply, int off, int cnt, long val)
	{
		off += cnt;
		
		while( 0 <= --cnt)
		{
			reply[--off] = (byte) val;
			val >>= 8;
		}
	}
	
	private static void s_setIntVal( byte[] buf, int off, int val)
	{
		buf[off++] = (byte) (val >> 24);
		buf[off++] = (byte) (val >> 16);
		buf[off++] = (byte) (val >> 8);
		buf[off]   = (byte) val;
	}
	
	private static void s_setShortVal( byte[] buf, int off, short val)
	{
		buf[off++] = (byte) (val >> 8);
		buf[off]   = (byte) val;				
	}
	
	private static InetSocketAddress s_getBroadcastAddress( int port) throws UnknownHostException
	{
		final byte[] BROADCAST_ADDR = { -1, -1, -1, -1};
		InetAddress ia = InetAddress.getByAddress( BROADCAST_ADDR);
		InetSocketAddress sa = new InetSocketAddress( ia, port);
		return sa;
	}
	
	private DhcpCfgEntry p_getEntryFromMac( long mac)
	{
		synchronized( m_DhcpEntries)
		{
			for( Iterator<DhcpCfgList> it = m_DhcpEntries.iterator(); it.hasNext();)
			{
				DhcpCfgList l = it.next();
				if (l.isResponsible( mac))
				{
					DhcpCfgEntry e = l.getKnownEntry(mac);
					if (null != e) return e;
				}
			}
		}
		return null;
	}
	
	private DhcpCfgEntry p_getAvailableEntry( long mac)
	{
		synchronized( m_DhcpEntries)
		{
			for( Iterator<DhcpCfgList> it = m_DhcpEntries.iterator(); it.hasNext();)
			{
				DhcpCfgList l = it.next();
				if (l.isResponsible( mac))
				{
					return l.getNewEntry(mac);
				}
			}
		}
		return null;
	}

	private DhcpCfgEntry p_getNewEntryFromMac( long mac)
	{
		DhcpCfgEntry e = p_getEntryFromMac( mac);
		if (null != e) return e;
		return p_getAvailableEntry(mac);
	}
	
	private DhcpCfgEntry p_getEntryFromIp4( long mac, int ip4)
	{
		synchronized( m_DhcpEntries)
		{
			for( Iterator<DhcpCfgList> it = m_DhcpEntries.iterator(); it.hasNext();)
			{
				DhcpCfgList l = it.next();
				if (l.isResponsible( mac))
				{
					DhcpCfgEntry e = l.getIp4Entry(ip4);
					if ((null != e) && (e.isMac(mac))) return e;
				}
			}
		}
		return null;
	}
	
	private DhcpCfgEntry p_getPrefferedIp( long mac, int ip4)
	{
		// first check if we have already an address for this mac
		DhcpCfgEntry e = p_getEntryFromMac( mac);
		if (null != e) return e;
		
		// second loog for ip4 address
		e = p_getEntryFromIp4( mac, ip4);
		if ((null != e) && ((e.isMac( mac) || e.isActive()))) return e;
		
		// last take the next available entry
		return p_getAvailableEntry( mac);
	}

	private boolean isMacAccepted( long mac)
	{
		Log.logDebug("searchhing for mac: " + MacUtil.s_getMacString(mac));
		for (DhcpCfgList el : m_DhcpEntries)
		{
			if (el.isResponsible(mac))
			{
				Log.logInfo( "DHCP-Server is responsable for mac " + MacUtil.s_getMacString(mac));
				return true;
			}
		}
		Log.logInfo( "No match for mac-address: " + MacUtil.s_getMacString(mac));
		return false;
	}
	
	private ArrayList<DhcpCfgList>	m_DhcpEntries;
	private IntfUiServer			m_UiServer;
	private long					m_UseLeasTimeMs;
	
	public static final int			DEFAULT_SERVER_PORT				= 67;
	public static final int			DEFAULT_CLIENT_PORT				= 68;
	
	public static final int			DEFAULT_IP4_LEASE_TIME			= 2*24*3600;
	public static final int			DEFAULT_IP4_RENEWAL_TIME		= 1*24*3600;
	public static final int			DEFAULT_IP4_REBIND_TIME			= 1*24*3600 + 18*3600;
	
	private static final int		DEFAULT_MAX_FRAME_SIZE			= 0x600;
}
