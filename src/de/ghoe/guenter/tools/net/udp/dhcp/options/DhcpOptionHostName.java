package de.ghoe.guenter.tools.net.udp.dhcp.options;

import de.ghoe.guenter.tools.net.udp.dhcp.options.base.DhcpOptionString;
import de.ghoe.guenter.tools.net.udp.dhcp.options.base.OptionParseInfo;

public class DhcpOptionHostName extends DhcpOptionString
{
	public DhcpOptionHostName( String param)
	{
		super( TYPE_ID, NAME_ID, param);
	}
	
	public DhcpOptionHostName( OptionParseInfo info) throws Exception
	{
		super(TYPE_ID, NAME_ID, info);
	}

	public static final Byte TYPE_ID										= 12;
	public static final String NAME_ID										= "HostName";
}
