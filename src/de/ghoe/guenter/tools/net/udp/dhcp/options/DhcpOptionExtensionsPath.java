package de.ghoe.guenter.tools.net.udp.dhcp.options;

import de.ghoe.guenter.tools.net.udp.dhcp.options.base.DhcpOptionString;
import de.ghoe.guenter.tools.net.udp.dhcp.options.base.OptionParseInfo;

public class DhcpOptionExtensionsPath extends DhcpOptionString
{
	public DhcpOptionExtensionsPath( String param)
	{
		super( TYPE_ID, NAME_ID, param);
	}
	
	public DhcpOptionExtensionsPath( OptionParseInfo info) throws Exception
	{
		super(TYPE_ID, NAME_ID, info);
	}

	public static final Byte TYPE_ID										= 18;
	public static final String NAME_ID										= "ExtensionsPath";
}
