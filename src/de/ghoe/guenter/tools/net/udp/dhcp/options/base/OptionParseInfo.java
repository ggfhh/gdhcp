package de.ghoe.guenter.tools.net.udp.dhcp.options.base;

public class OptionParseInfo
{
	public OptionParseInfo( byte[] buffer, int off, int len)
	{
		m_Buffer = buffer;
		m_Pos	 = off;
		m_Len	 = buffer.length - off;
		if (m_Len > len) m_Len = len;
	}
	
	public void close()
	{
		m_Pos = m_Len;
	}
	
	public int getOffset()
	{
		return m_Pos;
	}
	
	public boolean hasData()
	{
		return m_Pos < m_Len;
	}

	public byte readId( )
	{
		byte res = m_Buffer[ m_Pos++];
		return res;
	}
	
	public void skippData()
	{
		short len = readUnsignedByte();
		m_Pos += len;
	}
	
	public byte readByte( ) throws Exception
	{
		short len = readUnsignedByte();
		if (1 != len)
		{
			m_Pos += len;
			throw new Exception( "Unexpected payload size (expect 1 is " + len + ")");
		}
		byte res = m_Buffer[ m_Pos++];
		return res;
	}
	
	public byte[] readBytes()
	{
		short len = readUnsignedByte();
		byte[] res = new byte[ len];
		System.arraycopy( m_Buffer, m_Pos, res, 0, len);
		m_Pos += len;		
		return res;
	}
	
	public short readShort( ) throws Exception
	{
		short len = readUnsignedByte();
		if (2 != len) 
		{
			m_Pos += len;
			throw new Exception( "Unexpected payload size (expect 2 is " + len + ")");
		}
		short res = doReadShort();
		return res;
	}
	
	public short[] readShorts() throws Exception
	{
		short len = readUnsignedByte();
		if (0 != (len & 1))
		{
			m_Pos += len;
			throw new Exception( "Unexpected payload size (expect even bytes is " + len + ")");
		}
		int cnt = len / 2;
		short[] res = new short[ cnt];
		for (int i = 0; i < cnt; ++i)
		{
			res[i] = doReadShort();
		}		
		return res;		
	}
	
	public int readInt( ) throws Exception
	{
		int len = readUnsignedByte();
		if (4 != len) 
		{
			m_Pos += len;
			throw new Exception( "Unexpected payload size (expect 4 is " + len + ")");
		}
		int res = doReadInt();
		return res;
	}
	
	public int[] readInts() throws Exception
	{
		short len = readUnsignedByte();
		if (0 != (len & 3))
		{
			m_Pos += len;
			throw new Exception( "Unexpected payload size (expect multiple of 4 bytes is " + len + ")");
		}
		int cnt = len / 4;
		int[] res = new int[ cnt];
		for (int i = 0; i < cnt; ++i)
		{
			res[i] = doReadInt();
		}		
		return res;		
	}
	
	public long readLong( ) throws Exception
	{
		short len = readUnsignedByte();
		if (8 != len) 
		{
			m_Pos += len;
			throw new Exception( "Unexpected payload size (expect 1 is " + len + ")");
		}
		long res = doReadLong();
		return res;
	}
	
	public long[] readLongs() throws Exception
	{
		short len = readUnsignedByte();
		if (0 != (len & 7))
		{
			m_Pos += len;
			throw new Exception( "Unexpected payload size (expect multiple of 8 is " + len + ")");
		}
		int cnt = len / 8;
		long[] res = new long[ cnt];
		for (int i = 0; i < cnt; ++i)
		{
			res[i] = doReadLong();
		}		
		return res;		
	}
	
	public String readString()
	{
		short len = readUnsignedByte();
		String res = new String( m_Buffer, m_Pos, len);
		m_Pos += len;
		return res;
	}
	
	////////////////////////////////////////////////////////////////////////////////////
	private short readUnsignedByte()
	{
		short res = m_Buffer[m_Pos++];
		if (0 > res) res += 256;
		return res;
	}
	
	private short doReadShort()
	{
		short res = readUnsignedByte();
		res <<= 8;
		res  += readUnsignedByte();
		return res;
	}

	private int doReadInt()
	{
		int res = readUnsignedByte();
		for (int i = 0; i < 3; ++i)
		{
			res <<= 8;
			res  += readUnsignedByte();
		}
		return res;
	}
	
	private long doReadLong()
	{
		long res = readUnsignedByte();
		for (int i = 0; i < 7; ++i)
		{
			res <<= 8;
			res  += readUnsignedByte();
		}
		return res;
	}
			
	private byte[] m_Buffer;
	private int	   m_Pos;
	private int	   m_Len;
}
