package de.ghoe.guenter.tools.net.udp.dhcp.options.base;

import de.ghoe.guenter.tools.net.udp.dhcp.DhcpInfo;

public class DhcpOptionShort extends DhcpOption
{
	protected DhcpOptionShort( byte id, String name, short val)
	{
		super( id, name);
		m_Data = val;
	}
	
	protected DhcpOptionShort( byte id, String name, OptionParseInfo info) throws Exception
	{
		super( id, name);
		m_Data = info.readShort();
	}

	protected DhcpOptionShort( byte id, String name, String val) throws Exception
	{
		super( id, name);
		
		StringLongExtractor se = new StringLongExtractor( val);		
		m_Data = se.extractShort();
	}
	
	public short getValue()
	{
		return m_Data;
	}
	
	@Override
	public byte[] getBytes()
	{
		byte[] b = new byte[4];		
		b[0] = getId();
		b[1] = (byte) 2;
		b[2] = (byte) (m_Data >> 8);
		b[3] = (byte) (short) (m_Data);
		return b;
	}
	
	@Override
	public int getLen()
	{
		return 4;	/* id + len + data */
	}

	@Override
	public int insertData(byte[] buf, int pos)
	{
		buf[pos]   = getId();
		buf[++pos] = 2;
		DhcpInfo.setShort(buf, ++pos, m_Data);
		return 4;
	}
		
	private Short m_Data;
}
