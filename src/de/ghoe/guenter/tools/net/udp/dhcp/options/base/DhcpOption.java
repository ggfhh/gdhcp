package de.ghoe.guenter.tools.net.udp.dhcp.options.base;

public abstract class DhcpOption
{
	public enum Type { Type_Int, Type_String };

	protected DhcpOption( byte id, String name)
	{
		m_Id   = id;
		m_Name = name;
	}
		
	public byte getId()
	{
		return m_Id;
	}
	
	public String getName()
	{
		return m_Name;
	}
	
	public boolean lastOption()
	{
		return false;
	}
	
	public abstract byte[] getBytes();
	public abstract int	   getLen();
	public abstract int	   insertData( byte[] buf, int pos);
	
	protected static short s_getShort( byte v)
	{
		short res = v;
		
		if (0 > res) res += 256;		
		return res;		
	}
	
	private byte		m_Id;
	private String		m_Name;
}
