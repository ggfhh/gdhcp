package de.ghoe.guenter.tools.net.udp.dhcp.options;

import de.ghoe.guenter.tools.net.udp.dhcp.options.base.DhcpOptionShort;
import de.ghoe.guenter.tools.net.udp.dhcp.options.base.OptionParseInfo;

public class DhcpOptionBootFileSize extends DhcpOptionShort
{
	public DhcpOptionBootFileSize( short param)
	{
		super( TYPE_ID, NAME_ID, param);
	}
	
	public DhcpOptionBootFileSize( String in) throws Exception
	{
		super( TYPE_ID, NAME_ID, in);
	}
	
	public DhcpOptionBootFileSize( OptionParseInfo info) throws Exception
	{
		super(TYPE_ID, NAME_ID, info);
	}
	
	public int getFileSize()
	{
		int size = super.getValue();
		size *= 0x1000;
		return size;
	}
	
	public static final Byte TYPE_ID										= 13;
	public static final String NAME_ID										= "BootFileSize";
}
