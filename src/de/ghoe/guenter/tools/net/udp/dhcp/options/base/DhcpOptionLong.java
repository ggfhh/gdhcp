package de.ghoe.guenter.tools.net.udp.dhcp.options.base;

import de.ghoe.guenter.tools.net.udp.dhcp.DhcpInfo;

public class DhcpOptionLong extends DhcpOption
{
	protected DhcpOptionLong( byte id, String name, long val)
	{
		super( id, name);
		m_Data = val;
	}
	
	protected DhcpOptionLong( byte id, String name, OptionParseInfo info) throws Exception
	{
		super( id, name);
		m_Data = info.readLong();
	}
	
	protected DhcpOptionLong( byte id, String name, String val) throws Exception
	{
		super( id, name);
		
		StringLongExtractor se = new StringLongExtractor( val);		
		m_Data = se.extractLong();
	}
	
	public long s_getValue()
	{
		return m_Data;
	}
	
	@Override
	public byte[] getBytes()
	{
		byte[] b = new byte[10];		
		b[0] = getId();
		b[1] = 8;
		
		b[2] = (byte) (m_Data >> 56);
		b[3] = (byte) (m_Data >> 48);
		b[4] = (byte) (m_Data >> 40);
		b[5] = (byte) (m_Data >> 32);
		b[6] = (byte) (m_Data >> 24);
		b[7] = (byte) (m_Data >> 16);
		b[8] = (byte) (m_Data >>  8);
		b[9] = (byte) (long) (m_Data);
		return b;
	}
	
	@Override
	public int getLen()
	{
		return 10;	/* id + len + data */
	}

	@Override
	public int insertData(byte[] buf, int pos)
	{
		buf[pos]   = getId();
		buf[++pos] = 8;
		DhcpInfo.setLong(buf, ++pos, m_Data);
		return 10;
	}

	private Long m_Data;
}
