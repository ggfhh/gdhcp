package de.ghoe.guenter.tools.net.udp.dhcp.options.base;

import java.util.ArrayList;

import de.ghoe.guenter.tools.net.udp.dhcp.DhcpInfo;

public class DhcpOptionInts extends DhcpOption
{
	protected DhcpOptionInts( byte id, String name, int[] val)
	{
		super( id, name);
		m_Data = val;
	}
	
	protected DhcpOptionInts( byte id, String name, OptionParseInfo info) throws Exception
	{
		super( id, name);
		m_Data = info.readInts();
	}
	
	protected DhcpOptionInts( byte id, String name, String val) throws Exception
	{
		super( id, name);
		
		ArrayList<Integer> al = new ArrayList<Integer>();		
		StringLongExtractor se = new StringLongExtractor( val);		
		
		while(se.hasValues())
		{
			int v = se.extractInt();
			al.add( Integer.valueOf( v));
		}
		m_Data = new int[ al.size()];
		for (int i = 0; i < al.size(); ++i)
		{
			m_Data[i] = al.get(i);
		}
	}
	
	public int getValue( int idx)
	{
		return m_Data[idx];
	}
	
	public int[] getVal( )
	{
		return m_Data;
	}
	
	@Override
	public byte[] getBytes()
	{
		byte[] b = new byte[m_Data.length * 4 + 2];		
		b[0] = getId();
		b[1] = (byte) (m_Data.length * 4);
		
		int pos = 1;
		for (int i = 0; i < m_Data.length; ++i)
		{
			int d = m_Data[i];
			
			b[++pos] = (byte) (d >> 24);
			b[++pos] = (byte) (d >> 16);
			b[++pos] = (byte) (d >>  8);
			b[++pos] = (byte) (d);			
		}
		return b;
	}

	@Override
	public int getLen()
	{
		int len = 4 * m_Data.length;
		return 2 + len;	/* id + len + data */
	}

	@Override
	public int insertData(byte[] buf, int pos)
	{
		int len = m_Data.length * 4;
		buf[pos++]   = getId();
		buf[pos++] = (byte) len;
		
		for (int i = 0; i < m_Data.length; ++i)
		{
			DhcpInfo.setInt(buf, pos, m_Data[i]);
			pos += 4;
		}
		return len + 2;
	}

	private int[] m_Data;
}
