package de.ghoe.guenter.tools.net.udp.dhcp.options;

import de.ghoe.guenter.tools.net.udp.dhcp.options.base.DhcpOptionInt;
import de.ghoe.guenter.tools.net.udp.dhcp.options.base.OptionParseInfo;

public class DhcpOptionIpLeaseTime extends DhcpOptionInt
{
	public DhcpOptionIpLeaseTime( int param)
	{
		super( TYPE_ID, NAME_ID, param);
	}
	
	public DhcpOptionIpLeaseTime( OptionParseInfo info) throws Exception
	{
		super(TYPE_ID, NAME_ID, info);
	}
	
	public DhcpOptionIpLeaseTime( String in) throws Exception
	{
		super( TYPE_ID, NAME_ID, in);
	}

	public static final Byte TYPE_ID										= 51;
	public static final String NAME_ID										= "IpLeaseTime";
}
