package de.ghoe.guenter.tools.net.udp.dhcp;

public class DhcpInfo
{
	public static short getUnsignedByte( short b)
	{
		if (0 > b) b += 256;
		return b;
	}
	
	public static short getShort( byte[] buf, int off)
	{
		short res = getUnsignedByte( buf[off]);
		res <<= 8;
		res  += getUnsignedByte( buf[++off]);
		return res;
	}

	public static int getInt( byte[] buf, int off)
	{
		int res = getUnsignedByte( buf[off]);
		res <<= 8;
		res += getUnsignedByte( buf[++off]);
		res <<= 8;
		res += getUnsignedByte( buf[++off]);
		res <<= 8;
		res += getUnsignedByte( buf[++off]);
		return res;
	}
	
	public static long getLong( byte[] buf, int off)
	{
		return getLong( buf, off, 8);
	}
		
	public static long getLong( byte[] buf, int off, int cnt)
	{
		long res = getUnsignedByte( buf[off]);		
		while( 0 < --cnt)
		{
			res <<= 8;
			res += getUnsignedByte( buf[++off]);			
		}
		return res;
	}
	
	public static void setShort( byte[] buf, int off, short val)
	{
		buf[off]   = (byte) (val >> 8);
		buf[++off] = (byte) val; 
	}
	
	public static void setInt( byte[] buf, int off, int val)
	{
		buf[off]   = (byte) (val >> 24);
		buf[++off] = (byte) (val >> 16);
		buf[++off] = (byte) (val >> 8);
		buf[++off] = (byte) val; 
	}
	
	public static void setLong( byte[] buf, int off, long val)
	{
		buf[off]   = (byte) (val >> 56);
		buf[++off] = (byte) (val >> 48);
		buf[++off] = (byte) (val >> 40);
		buf[++off] = (byte) (val >> 32);
		buf[++off] = (byte) (val >> 24);
		buf[++off] = (byte) (val >> 16);
		buf[++off] = (byte) (val >> 8);
		buf[++off] = (byte) val; 
	}
	
	
	
	
	public static final byte	DHCP_REQUEST				= 1;
	public static final byte	DHCP_REPLY					= 2;
	
	public static final byte	DHCP_ADDRLEN				= 6;
	
	public static final byte	DHCP_FILLBYTE				= 0;
	
	public static final int		DHCP_MAGIC_COOKIE			= 0x63825363;

	public static final int		BOOTP_PACKET_SERVERNAME_LEN	= 64;
	public static final int		BOOTP_PACKET_FILENAME_LEN	= 128;
	public static final int		BOOTP_PACKET_CHIADDR_LEN	= 16;

	public static final int		BOOTP_POS_OP				= 0;
	public static final int		BOOTP_POS_HTYPE				= BOOTP_POS_OP 					+ 1;
	public static final int		BOOTP_POS_HLEN				= BOOTP_POS_HTYPE 				+ 1;
	public static final int		BOOTP_POS_HOPS				= BOOTP_POS_HLEN 				+ 1;
	public static final int		BOOTP_POS_XID				= BOOTP_POS_HOPS 				+ 1;
	public static final int		BOOTP_POS_SECS				= BOOTP_POS_XID 				+ 4;
	public static final int		BOOTP_POS_FLAGS				= BOOTP_POS_SECS 				+ 2;
	public static final int		BOOTP_POS_CIADDR			= BOOTP_POS_FLAGS				+ 2;
	public static final int		BOOTP_POS_YIADDR			= BOOTP_POS_CIADDR				+ 4;
	public static final int		BOOTP_POS_SIADDR			= BOOTP_POS_YIADDR				+ 4;
	public static final int		BOOTP_POS_GIADDR			= BOOTP_POS_SIADDR				+ 4;
	public static final int		BOOTP_POS_CHIADDR			= BOOTP_POS_GIADDR				+ 4;
	public static final int		BOOTP_POS_SNAME				= BOOTP_POS_CHIADDR				+ BOOTP_PACKET_CHIADDR_LEN;
	public static final int		BOOTP_POS_FILE				= BOOTP_POS_SNAME 				+ BOOTP_PACKET_SERVERNAME_LEN;
	public static final int		DHCP_POS_MCOOKI				= BOOTP_POS_FILE 				+ BOOTP_PACKET_FILENAME_LEN;
	public static final int		DHCP_POS_OPTIONS			= DHCP_POS_MCOOKI				+ 4;

	public static final int		BOOTP_MIN_PACKET_SIZE		= BOOTP_POS_SNAME;
	public static final int		BOOTP_PACKET_SIZE			= BOOTP_MIN_PACKET_SIZE + BOOTP_PACKET_SERVERNAME_LEN + BOOTP_PACKET_FILENAME_LEN;

	public static final int		DHCP_MAGIC_COOKIE_SIZE		= 4;
	public static final int		DHCP_MIN_PACKET_SIZE		= BOOTP_PACKET_SIZE + DHCP_MAGIC_COOKIE_SIZE; 
}
