package de.ghoe.guenter.tools.net.udp;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;

import de.ghoe.guenter.support.Log;

public abstract class UdpServer extends UdpReceiveThread 
	implements IntfUdpServer, IntfUdpReceiver
{
	protected UdpServer( String name, int port, int bufSize)
	{
		super( name, bufSize);
		
		m_Name = name;
		m_ServerPort = port;
	}
	
	@Override
	public void open( ) throws Exception
	{
		InetSocketAddress sa = new InetSocketAddress( m_ServerPort);		
		startUdpReceive( this, sa, null, 0, null, true);
	}
	
	@Override
	public void openOnDevName( String devName) throws Exception
	{
		NetworkInterface ni = NetworkInterface.getByName( devName);
		InetAddress ia = ni.getInetAddresses().nextElement();
		InetSocketAddress sa = new InetSocketAddress( ia, m_ServerPort);
		startUdpReceive( this, sa, null, 0, null, true);
	}
	
	@Override
	public void openOnDevIdx( int devIdx) throws Exception
	{
		NetworkInterface ni = NetworkInterface.getByIndex( devIdx);
		if (null == ni) throw new Exception( "Device index " + devIdx + " is not accessable");
		InetAddress ia = ni.getInetAddresses().nextElement();
		InetSocketAddress sa = new InetSocketAddress( ia, m_ServerPort);
		startUdpReceive( this, sa, null, 0, null, true);
	}
	
	@Override
	public void openOnAddress( String ipAddr) throws Exception
	{
		InetAddress ia = InetAddress.getByName( ipAddr);
		InetSocketAddress sa = new InetSocketAddress( ia, m_ServerPort);
		Log.logInfo("Starting UDP-Server at address: " + sa.getHostString() + ':' + sa.getPort());
		startUdpReceive( this, sa, null, 0, null, true);
	}

	@Override
	public void shutdown()
	{
		shutdownUdpReceive();
	}

	@Override
	public int getPort()
	{
		return m_ServerPort;
	}
	
	@Override
	public int  getSocketPort()
	{
		return m_ServerPort;
	}
	
	@Override
	public void setPort( int port)
	{
		m_ServerPort = port;
	}
	
	@Override
	public String getName()
	{
		return m_Name;
	}
	
	//////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////	

	private String					m_Name;
	protected Log				m_Log;
	protected int					m_ServerPort;
}
