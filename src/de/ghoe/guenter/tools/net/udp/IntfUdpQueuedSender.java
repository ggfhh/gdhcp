package de.ghoe.guenter.tools.net.udp;

import java.net.DatagramPacket;

public interface IntfUdpQueuedSender
{
	public DatagramPacket getPacketToQueue( );
	
}
