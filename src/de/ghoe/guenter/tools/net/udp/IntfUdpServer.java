package de.ghoe.guenter.tools.net.udp;


public interface IntfUdpServer 
{
	public void open( ) throws Exception;
	public void openOnDevName( String devName) throws Exception;
	public void openOnDevIdx( int devIdx) throws Exception;
	public void openOnAddress( String ipAddr) throws Exception;
	
	public void shutdown( ) throws Exception;

	public void setPort( int port);
	public int  getPort( );
	public int  getSocketPort();
	public String getName();
	
}
