package de.ghoe.guenter.tools.net.udp.support;

import java.net.DatagramPacket;

public class UdpQueuedPacket
{
	public UdpQueuedPacket( DatagramPacket p)
	{
		m_Packet       = p;
		m_LastSendTime = System.currentTimeMillis();
		m_SendIdx     = 0;
	}
	
	public DatagramPacket getPacket()
	{
		return m_Packet;
	}

	public int getSendIdx()
	{
		return m_SendIdx;
	}
	
	public int incSendIdx()
	{
		m_LastSendTime = System.currentTimeMillis();
		return m_SendIdx++;
	}
	
	public long getLastTime()
	{
		return m_LastSendTime;
	}
	
	public boolean isTimeoutOver( long ct, int[] toMs)
	{
		if (m_SendIdx <= toMs.length)
		{
			long to = toMs[m_SendIdx];
			return to < (ct - m_LastSendTime);
		}
		return true;
	}
	
	private DatagramPacket 			m_Packet;
	private long					m_LastSendTime;
	private int						m_SendIdx;
}
