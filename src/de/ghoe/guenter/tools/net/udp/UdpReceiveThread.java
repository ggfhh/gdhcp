package de.ghoe.guenter.tools.net.udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;

import de.ghoe.guenter.support.Log;
import de.ghoe.guenter.tools.net.support.Ip4Util;

public class UdpReceiveThread
	implements Runnable
{
	public UdpReceiveThread( String name, int bufSize)
	{
		Log.logEvent( "Created UdpReceiveThread with name " + name);
		m_Name = name;
		m_RxBufSize = bufSize;
	}
	
	public int getUsedIpV4()
	{
		int res = 0;
		if (null != m_Sock)
		{
			InetAddress ia = m_Sock.getLocalAddress();
			res = Ip4Util.s_getIp4Addr(ia);
		}
		return res;
	}

	public boolean udpRxStartup()
	{
		Log.logEvent( "UdpRxStartup called");
		return true;
	}

	public boolean udpRxTimeout()
	{		
		Log.logError( "UdpRxTimeout called");
		return true;
	}
	
	public void udpRxAbort()
	{
		Log.logError( "UdpRxAbort called");		
	}
	
	public void udpRxShutdown()
	{		
		Log.logError( "UdpRxShutdown called");
	}
	
	public boolean isUdpReceiverUp()
	{
		return null != m_Receiver;
	}
	
	//////////////////////////////////////////////////////////////////////////////////
	// Runnable 
	//////////////////////////////////////////////////////////////////////////////////	
	@Override
	public void run()
	{
		IntfUdpReceiver r = m_Receiver;
		
//		m_Log.logInfo( "Running UDP-Receive-Thread!");
		if ((null != r) && (r.udpRxStartup()))
		{
			try
			{
				byte[] buf = new byte[ m_RxBufSize];
				
				for (;;)
				{	// Handle until timeout is reached (to provide delay'ed ack's
					if (null != m_RxTimeouts)
					{
						if (m_RxTimeoutIdx >= m_RxTimeouts.length) throw new SocketTimeoutException( "Timeout limit reached!");
						m_Sock.setSoTimeout( m_RxTimeouts[m_RxTimeoutIdx++]);						
					}
					
					try
					{
						DatagramPacket p = new DatagramPacket( buf, buf.length);					
						m_Sock.receive( p);
						m_RxTimeoutIdx = 0;	// received -> restart timeout
						if (!r.udpRxHandle(p))
							break;
					}
					catch( SocketTimeoutException e_to)
					{
						Log.log( "RxTimeout-", e_to);
						if (!r.udpRxTimeout())
							break;
					}
				}
			}
			catch( Throwable e)
			{
				if (!m_RxThread.isInterrupted())
				{
					Log.log( m_Name + ": Failed to receive from Socket", e);
				}
				else
				{
					Log.logEvent( m_Name + ": Server closed");
				}
			}
		}
		r.udpRxShutdown();
		Log.logInfo( "UDP-Receive-Thread ready!");
		try
		{
			Thread.sleep( KEEP_RUNNING_TIME_MS);
		}
		catch (InterruptedException e)
		{
			Log.log( "Failed to wait for UdpRxThread closed!", e);
		}		
		udpRxClose();
		Log.logInfo( "UDP-Receive-Thread closed!");
	}

	//////////////////////////////////////////////////////////////////////////////////
	//  IntfUdpReceive
	//////////////////////////////////////////////////////////////////////////////////
	protected void startUdpReceive( IntfUdpReceiver rx, SocketAddress sa, SocketAddress connectAddr, int queueDepth, int rxToMs[], boolean acceptBroadcast) throws Exception
	{
		m_RxTimeouts = rxToMs;
		m_RxTimeoutIdx = 0;
		
		if (null == m_Sock)
		{
			Log.logEvent( "Create UDP-Socket with queue length " + queueDepth);
			m_Sock = (null != sa) ? new UdpQueuedSocket( queueDepth, sa):
								    new UdpQueuedSocket( queueDepth);
			Log.logVerbose("Enable braodcast");
			if (acceptBroadcast) m_Sock.setBroadcast(true);			
			Log.logVerbose("Connect socket");
			if (null != connectAddr) m_Sock.connect(connectAddr);
		}
		m_Receiver = rx;		
		if (null == m_RxThread)
		{
			Log.logVerbose("Connect socket");
			m_RxThread = new Thread( this);
			Log.logVerbose("Start Rx-Thread");
			m_RxThread.start();			
		}
		Log.logEvent( m_Name + "-Receiver started on if=" + m_Sock.getLocalAddress().getHostAddress() + ":" + m_Sock.getLocalPort());
	}
	
	protected void shutdownUdpReceive()
	{
		Log.logEvent( "Shutdown receive-thread: " + m_Name);
		if (null != m_RxThread)
		{
			m_Receiver = null;
			m_RxThread.interrupt();
			closeSocket();
			try
			{
				m_RxThread.join();
				Log.logEvent( "Receiver is down!");
			}
			catch( Throwable e)
			{
				Log.log( m_Name + ": Failed to join Rx-Thread", e);
			}
			m_RxThread = null;
		}
		else
		{
			closeSocket();
		}
	}
	
	protected void udpRxClose()
	{	// currently noting to do
	}
	
	//////////////////////////////////////////////////////////////////////////////////
	// protected 
	//////////////////////////////////////////////////////////////////////////////////
	protected void sendPacketUnqueued( DatagramPacket p) throws IOException
	{
//		m_Log.logVerbose( "Send-unqueued Datagram packet with " + p.getLength() + " bytes");
		m_Sock.sendUnqueued(p);
	}
	
	protected boolean canEnqueuePacket()
	{
		return m_Sock.canEnqueue();
	}
	
	protected boolean isSendFinishe()
	{
		return m_Sock.isSendFinished();
	}
	
	protected void sendPacket( DatagramPacket p) throws IOException
	{
		Log.logVerbose( "Send Datagram packet with " + p.getLength() + " bytes");		
		Log.logWarning( "Skipp send of " + p.getLength() + " bytes to " + p.getAddress().toString());
		Log.logVerbose( "Send to: " + p.getAddress().toString());
		Log.logVerbose( "Using socket: " + m_Sock.getLocalAddress().toString() + " using port " + m_Sock.getLocalPort());
		m_Sock.send( p);
	}
	
	protected void resendPackets() throws IOException
	{
		m_Sock.resendPackets();
	}
		
	protected void confirmPacket()
	{
//		m_Log.logVerbose( "Confirm packet");
		m_Sock.confirmPacket();
	}
	
	protected void bind( SocketAddress sa) throws SocketException
	{
		if ((!m_Sock.isBound()) || (!sa.equals( m_Sock.getRemoteSocketAddress())))
		{	// only bind 1. time -> address should not change
			m_Sock.bind( sa);			
		}
	}
	
	protected void connect( SocketAddress sa) throws SocketException
	{
//		m_Log.logInfo( "Connect socket " + m_Sock.getLocalSocketAddress().toString() + " to address " + sa.toString());		
		m_Sock.connect( sa);
	}
	
	//////////////////////////////////////////////////////////////////////////////////
	// private 
	//////////////////////////////////////////////////////////////////////////////////	
	private void closeSocket()
	{
		if (null != m_Sock)
		{
			try
			{
				m_Sock.close();
			}
			catch( Throwable e)
			{
				Log.log( m_Name + ": Failed to close socket", e);
			}			
			m_Sock = null;
		}		
	}

	private	IntfUdpReceiver 		m_Receiver; 
	private String					m_Name;
	private Thread 					m_RxThread;
	private int						m_RxBufSize;
	private UdpQueuedSocket			m_Sock;
	
	private int[]					m_RxTimeouts;
	private int						m_RxTimeoutIdx;
	
	private static final int		KEEP_RUNNING_TIME_MS = 10000;
}
