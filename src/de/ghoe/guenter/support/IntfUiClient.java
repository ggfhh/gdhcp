package de.ghoe.guenter.support;

public interface IntfUiClient
{
	boolean uiStartClient();
	boolean uiUpdateClient();
	void uiStopClient();
	
	boolean uiIsClientStarted();
}
