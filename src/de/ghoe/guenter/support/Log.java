package de.ghoe.guenter.support;

public abstract class Log
{
	public Log()
	{}

	public enum Level { Off, Error, Warning, Event, Output, Info, Verbose, Debug, Extended};
	
	public static void s_setLog( Log log, Level level)
	{
		s_Level = (null != log) ? level : Level.Off;
		s_Instance = log;		
	}
	
	public static boolean s_isLogOn( Level l)
	{
		return (l.ordinal() <= s_Level.ordinal());
	}
	
	public static void logError( String msg)
	{
		plog( Level.Error, msg);
	}
	public static void logWarning( String msg)
	{
		plog( Level.Warning, msg);
	}
	public static void logEvent( String msg)
	{
		plog( Level.Event, msg);
	}
	public static void log( String msg)
	{
		plog( Level.Output, msg);
	}
	public static void logInfo( String msg)
	{
		plog( Level.Info, msg);
	}
	public static void logVerbose( String msg)
	{
		plog( Level.Verbose, msg);
	}
	public static void logDebug( String msg)
	{
		plog( Level.Debug, msg);
	}
	public static void logExtended( String msg)
	{
		plog( Level.Extended, msg);
	}
	
	public static void logHex( String msg, byte[] data)
	{
		logHex( msg, data, data.length, 0);
	}
	public static void logHex( String msg, byte[] data, int cnt)
	{
		logHex( msg, data, cnt, 0);
	}
	public static void logHex( String msg, byte[] data, int cnt, int off)
	{
		if (0 == off)
			plog( Level.Verbose, msg + '[' + cnt + " bytes]");
		else
			plog( Level.Verbose, msg + '[' + cnt + " bytes off=" + off + ']');			
		
		plog( Level.Verbose, "================================================================================");
		if (0 > off) off = 0;
		if (cnt > data.length) cnt = data.length;		
		for (int i = off; i < cnt; i += LINE_LENGTH)
		{
			StringBuilder out = new StringBuilder( String.format("0x%08x:",i));
			for (int j = 0; j < LINE_LENGTH; ++j)
			{
				out.append( String.format(" %02x", data[i+j]));
			}
			plog( Level.Verbose, out.toString());			
		}		
	}
	
	public static void log( String msg, Throwable e)
	{
		plog( Level.Error, msg + " Catched: " + e.getLocalizedMessage());
	}
	
	protected abstract void logImpl( Level l, String msg);
		
	protected static String s_getLevelString( Level l)
	{
		switch( l)
		{
		case Off: return "";	// no output done in case of log is off :-)
		case Error: return "Error";
		case Warning: return "Warning";
		case Event: return "Event";
		case Output: return "Output";
		case Info: return "Info";
		case Verbose: return "Verbose";
		case Debug: return "Debug";
		case Extended: return "Extended";
		default: return "<unknown log-level>";
		}
	}
	
	private static void plog( Level l, String msg)
	{
		if (l.ordinal() <= s_Level.ordinal())
		{
			StackTraceElement[] stack = Thread.currentThread().getStackTrace();
			String postfix = "";
			if (stack.length > 3)				
			{
				StackTraceElement e = stack[3]; 
				postfix = " -> " + e.getClassName() + '.' + e.getMethodName() + '[' + e.getFileName() + ':' + e.getLineNumber() + ']';
			}
			s_Instance.logImpl( l, msg + postfix);
		}
	}
	
	private static final int LINE_LENGTH = 32;
	private static Level s_Level = Level.Off;
	private static Log s_Instance;	
}
