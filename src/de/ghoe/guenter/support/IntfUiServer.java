package de.ghoe.guenter.support;

import java.util.ArrayList;

import de.ghoe.guenter.tool.dhcp.MacConfig;

public interface IntfUiServer
{
	void registerUiClient( IntfUiClient client);

	int getIpStart();
	int getIpEnd();
	ArrayList<MacConfig> getMacs();
}
