package de.ghoe.guenter.support;

public class LibLoader
{
	public static LibLoader create( String libExtractPath)
	{
		if (null == s_Instance)
		{
			createInstanceSave( libExtractPath);
		}
		return s_Instance;
	}
	
	private static synchronized boolean createInstanceSave( String libExtractPath)
	{
		boolean res = (null == s_Instance); 
		if (res)
		{
			s_Instance = new LibLoader( libExtractPath);
		}
		return res;
	}
	
	private LibLoader( )
	{		
	}
	
	private LibLoader( String libExtractPath)
	{		
	}
	
	private static LibLoader	s_Instance = null;
}
