package de.ghoe.guenter.support;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Semaphore;

public class JobQueue extends Thread
{
	public JobQueue()
	{
		ArrayList<IntfJobQueueEntry> al = new ArrayList<IntfJobQueueEntry>();
		m_Jobs = Collections.synchronizedList( al);
		al = new ArrayList<IntfJobQueueEntry>();
		m_HighPrioJobs = Collections.synchronizedList( al);
		m_JobSem = new Semaphore(0);
		start();
	}
	
	public void shutdown()
	{
		pushJob( null);
		try
		{
			join( 5000);
		}
		catch (InterruptedException e)
		{
			Log.log("Failed to join job-thread", e);
		}
	}
	
	public void pushHighPrioJob( IntfJobQueueEntry job)
	{
		synchronized (this)
		{
			m_HighPrioJobs.add( job);
			m_JobSem.release();
		}
	}
	
	public void pushJob( IntfJobQueueEntry job)
	{
		synchronized (this)
		{
			m_Jobs.add( job);
			m_JobSem.release();
		}
	}
	
	public  void clear()
	{
		synchronized (this)
		{
			int val = m_JobSem.availablePermits();
			m_JobSem.tryAcquire( val);
			m_HighPrioJobs.clear();
			m_Jobs.clear();
		}
	}
	
	public void run()
	{
		try
		{
			for (;;)
			{				
				m_JobSem.acquire();
				
				IntfJobQueueEntry job;				
				synchronized(this)
				{
					if (!m_HighPrioJobs.isEmpty())
						job = m_HighPrioJobs.remove(0);
					else if (!m_Jobs.isEmpty())
						job = m_Jobs.remove(0);
					else
						continue;
				}
				if (null == job) break;	// end of processing reached
				
				job.process();
			}
		}
		catch( Throwable e)
		{
			Log.log( "Failed to run job", e);
		}
	}

	private List<IntfJobQueueEntry>		m_HighPrioJobs;
	private List<IntfJobQueueEntry>		m_Jobs;
	private Semaphore					m_JobSem;
}
