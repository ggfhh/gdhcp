package de.ghoe.guenter.tool.dhcp;

import de.ghoe.guenter.support.Log;

public class ConsoleLog extends Log
{
	public ConsoleLog()
	{
//		Log.s_setLog( this, Level.Output);
		Log.s_setLog( this, Level.Extended);
	}
	
	@Override
	protected void logImpl(Level l, String msg)
	{
		if (l.ordinal() <= Log.Level.Warning.ordinal())
		{
			System.err.printf("%s: %s\n", Log.s_getLevelString(l), msg);
		}
		else
		{
			System.out.printf("%s: %s\n", Log.s_getLevelString(l), msg);
		}
	}
}
