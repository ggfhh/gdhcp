package de.ghoe.guenter.tool.dhcp;

public class MacConfig
{
	public MacConfig( long mac)
	{
		m_MacStart = mac;
		m_MacEnd = mac;
	}
	
	public void setEnd( long end)
	{
		m_MacEnd = end;
	}
	
	public long getStart()
	{
		return m_MacStart;
	}

	public long getEnd()
	{
		return m_MacEnd;
	}

	private long m_MacStart;
	private long m_MacEnd;
}
