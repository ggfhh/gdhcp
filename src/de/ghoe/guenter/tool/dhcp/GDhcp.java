package de.ghoe.guenter.tool.dhcp;

import java.util.ArrayList;

import de.ghoe.guenter.support.IntfUiClient;
import de.ghoe.guenter.support.IntfUiServer;
import de.ghoe.guenter.support.Log;
import de.ghoe.guenter.tools.net.support.Ip4Util;
import de.ghoe.guenter.tools.net.support.MacUtil;
import de.ghoe.guenter.tools.net.udp.dhcp.DhcpServer;

public class GDhcp implements IntfUiServer 
{
	public GDhcp( String args[]) throws Exception
	{
		m_Macs = new ArrayList<MacConfig>();

		for (String s : args)
		{
			String v = s_getValue( s, "Ip4=");
			if (null != v)  
			{
				m_ip4Start = Ip4Util.s_getIp4Addr(v);
				if (0 == m_ip4End) m_ip4End = m_ip4Start +1;
				continue;
			}
			v = s_getValue( s, "Ip4E=");
			if (null != v)  
			{
				m_ip4End = Ip4Util.s_getIp4Addr(v);
				continue;
			}
			v = s_getValue( s, "Mac=");
			if (null != v)  
			{
				m_Macs.add( new MacConfig( MacUtil.s_getMacAddr(v)));
				continue;
			}
			v = s_getValue( s, "MacE=");
			if (null != v)  
			{
				if (m_Macs.isEmpty())
				{
					m_Macs.add( new MacConfig( MacUtil.s_getMacAddr(v)));
				}
				else
				{
					m_Macs.get( m_Macs.size()-1).setEnd(MacUtil.s_getMacAddr(v));
				}
				continue;
			}
			v = s_getValue( s, "NetIf=");
			if (null != v)  
			{
				this.m_IfIp = v;
				continue;
			}
			v = s_getValue( s, "ServerIp=");
			if (null != v)
			{
				this.m_ServerIp = Ip4Util.s_getIp4Addr(v);
				continue;
			}
			Log.logError( String.format("Unknown imput parameter '%s' used. Call: GDhcp [Ip4=<x>] [Ip4E=<x>] [Mac=<x>] [MacE=<x>] [NetIf=<x>]", s));			
		}
		Log.log( "Configuration of GDhcp:\n" +
				 "================================================\n" +
				 "Ip4-Start=" + Ip4Util.s_getIp4String( m_ip4Start) + "\n" +
				 "Ip4-End  =" + Ip4Util.s_getIp4String( m_ip4End) + "\n");
				 for (MacConfig m : m_Macs)
				 {
					 Log.log( "MAC-Start=" + MacUtil.s_getMacString( m.getStart()) + "\n" +
							  "MAC-End  =" + MacUtil.s_getMacString( m.getEnd()) + "\n");
				 }
		Log.log( "Net-IF   =" + m_IfIp + "\n" +
				 "Server   =" + Ip4Util.s_getIp4String( m_ServerIp) + "\n"
				 );
	}
	
	@Override
	public void registerUiClient(IntfUiClient client)
	{
		m_Client = client;
	}

	@Override
	public int getIpStart()
	{
		return m_ip4Start;
	}

	@Override
	public int getIpEnd()
	{
		return m_ip4End;
	}

	@Override
	public ArrayList<MacConfig> getMacs()
	{
		return m_Macs;
	}

	public static void main( String[] args)
	{
		new ConsoleLog();
		Log.log("Starting GDhcp-Server!");
		try
		{
			GDhcp app = new GDhcp( args);
			
			app.start();
		}
		catch(Throwable e)
		{
			Log.log( "Failed to process DHCP", e);
		}
	}
	
	void start() throws Exception
	{		
		DhcpServer server = new DhcpServer( this, 60*1000);
		server.openOnAddress(m_IfIp);
	}
	
	private String s_getValue( String in, String tag)
	{
		if (in.startsWith(tag))
			return in.substring( tag.length());
		return null;
	}
	
	private IntfUiClient m_Client;
	private String	m_IfIp = "192.168.24.24";
	
	private ArrayList<MacConfig>	m_Macs;
	private int		m_ServerIp = 0;
	private int		m_ip4Start = 0;
	private int		m_ip4End   = 0;
}
